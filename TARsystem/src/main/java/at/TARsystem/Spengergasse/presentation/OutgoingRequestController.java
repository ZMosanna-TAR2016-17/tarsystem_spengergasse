package at.TARsystem.Spengergasse.presentation;

import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.facade.LessonShiftFacade;
import at.TARsystem.Spengergasse.facade.commands.CreateAbsenceRequestCommand;
import at.TARsystem.Spengergasse.facade.commands.CreateLessonShiftRequestCommand;
import at.TARsystem.Spengergasse.facade.views.LessonShiftRequestView;
import at.TARsystem.Spengergasse.facade.views.UserView;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Controller
@ControllerAdvice
@RequestMapping(value = "/")
public class OutgoingRequestController
{

    private final LessonShiftFacade lsFacade;

    @ModelAttribute("currentUser")
    public UserView getCurrentUser()
    {
        // return UserView.of("mos15809", "mos15809@spengergasse.at", "Zeynab", "Mosanna", "MOS", UserRole.Lehrer);
        return UserView.of("berger","berger@spengergasse.at","Franz","Berger","BF",UserRole.AV);
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView getLoginPage()
    {
        return new ModelAndView("/login");
    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(ModelAndView mv)
    {
        mv.getModelMap().addAttribute("currentUser", Optional.empty());
        mv.setViewName("redirect:/login");
        return mv;
    }

    @RequestMapping(value = "/antraege/{userName}", method = RequestMethod.GET)
    public ModelAndView showWorkDesk(@PathVariable String userName, ModelAndView mv, BindingResult bindingResult)
    {
        mv.setViewName("antrag/workDesk");
        List<LessonShiftRequestView> lessonShiftRequestViewList = lsFacade.getAllLessonShiftsOfUser(userName);

        mv.getModelMap().addAttribute("lessonShifts", lessonShiftRequestViewList);
        return mv;
    }

    @RequestMapping(value = "/addAntrag", method = RequestMethod.GET)
    public ModelAndView showAddAbsenceRequestForm(ModelAndView mv,
                                                  BindingResult bindingResult)
    {
        mv.setViewName("/antrag/mainform");
        mv.getModelMap().addAttribute("absenceRequest", CreateAbsenceRequestCommand
                .of(getCurrentUser().getUsername(), LocalDateTime.now(), LocalDate.now()));
        return mv;
    }

    @RequestMapping(value = "/addAntrag", method = RequestMethod.POST)
    public ModelAndView handleSubmitAddAbsenceRequest(@RequestParam String action,
                                                      @Valid @ModelAttribute("absenceRequest")
                                                      CreateAbsenceRequestCommand command,
                                                      BindingResult bindingResult, ModelAndView mv)
    {
        if (bindingResult.hasErrors())
        {
            mv.setViewName("/antrag/mainform");
            return mv;
        }
        lsFacade.setArCreateCommand(command);
        if (action.equalsIgnoreCase("Stundentausch")) mv.setViewName("redirect:/addStundentausch");
        return mv;
    }

    @RequestMapping(value = "/addStundentausch", method = RequestMethod.GET)
    public ModelAndView showAddLessonShiftForm(ModelAndView mv)
    {
        mv.setViewName("antrag/stundentausch_form");
        mv.getModelMap().addAttribute("mainRequest", lsFacade.getArCreateCommand());
        mv.getModelMap()
          .addAttribute("lessonShift", CreateLessonShiftRequestCommand.of("", 0, 0, LocalDate.now(), 0, 0));
        mv.getModelMap().addAttribute("classNames",lsFacade.allSchoolClasses());
        mv.getModelMap().addAttribute("courseHoursFrom",lsFacade.getCoursHours());
        mv.getModelMap().addAttribute("courseHoursTo",lsFacade.getCoursHours());
        mv.getModelMap().addAttribute("freeHoursFrom", lsFacade.getCoursHours());
        mv.getModelMap().addAttribute("freeHoursTo", lsFacade.getCoursHours());
        return mv;
    }

    @RequestMapping(value = "/addStundentausch", method = RequestMethod.POST)
    public ModelAndView handleAddLessonShiftForm(
            @Valid @ModelAttribute("lessonShift") CreateLessonShiftRequestCommand command, BindingResult bindingResult,
            ModelAndView mv)
    {
        if (bindingResult.hasErrors())
        {
            mv.setViewName("antrag/stundentausch_form");
            return mv;
        }
        LessonShiftRequestView view = lsFacade.saveLessonShift(command);
        if (view.getMainRequest().isResult() && view.getLessonShiftViwe().isResult())
        {
            mv.setViewName("redirect:/antraege/" + view.getMainRequest().getRequester());
        }
        else
        {
            // redirect to form for error handling
            mv.setViewName("antrag/stundentausch_form");
        }
        return mv;
    }
}
