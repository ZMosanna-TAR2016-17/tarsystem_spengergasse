package at.TARsystem.Spengergasse.presentation;

import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.facade.LessonShiftFacade;
import at.TARsystem.Spengergasse.facade.commands.AcceptOrDenyCommand;
import at.TARsystem.Spengergasse.facade.commands.CreateStatusCommand;
import at.TARsystem.Spengergasse.facade.views.LessonShiftRequestView;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Controller
@ControllerAdvice
@RequestMapping(value = "/anforderungen")
public class IncomingRequestsController
{
    private final LessonShiftFacade lsFacade;

    @RequestMapping(value = "/{userName}", method = RequestMethod.GET)
    public ModelAndView showWorkDesk(@PathVariable String userName, ModelAndView mv, BindingResult bindingResult)
    {
        mv.setViewName("anforderung/inbox");
        List<LessonShiftRequestView> lessonShiftRequestViewList = lsFacade.getAllLessonShiftsWaitForUser(userName);
        mv.getModelMap().addAttribute("lessonShifts", lessonShiftRequestViewList);
        return mv;
    }

    @RequestMapping(value = "/acceptStundentausch/{id}/{userName}", method = RequestMethod.GET)
    public ModelAndView acceptLessonshift(@PathVariable Long id, @PathVariable String userName, ModelAndView mv)
    {
        UserRole userRole = (userName == lsFacade.whoIsDirector()) ? UserRole.SL : (userName == lsFacade
                .whoIsTimeTableAdmin()) ? UserRole.SPV : UserRole.AV;
        AcceptOrDenyCommand acceptCommand = AcceptOrDenyCommand
                .of(id, CreateStatusCommand.of(LocalDateTime.now(), userName, userRole, Decision.akzeptiert));
        if (!lsFacade.acceptOrDenyLessonShift(acceptCommand).equalsIgnoreCase("ERROR"))
        {
            log.warn("Antrag ist erfolgreich akzeptiert");
        }
        else
        {
            log.warn("ERROR: Antrag ist nicht akzeptiert");
        }
        mv.setViewName("redirect:/anforderungen/" + userName);
        return mv;
    }

    @RequestMapping(value = "/denyStundentausch/{id}/{userName}", method = RequestMethod.GET)
    public ModelAndView showWorkDesk(@PathVariable Long id, @PathVariable String userName, ModelAndView mv)
    {
        UserRole userRole = (userName.equalsIgnoreCase(lsFacade.whoIsDirector())) ? UserRole.SL : (userName
                .equalsIgnoreCase(lsFacade.whoIsTimeTableAdmin())) ? UserRole.SPV : UserRole.AV;
        AcceptOrDenyCommand denyCommand = AcceptOrDenyCommand
                .of(id, CreateStatusCommand.of(LocalDateTime.now(), userName, userRole, Decision.abgelehnt));
        if (!lsFacade.acceptOrDenyLessonShift(denyCommand).equalsIgnoreCase("ERROR"))
        {
            log.warn("Antrag ist erfolgreich abgelehnt");
        }
        else
        {
            log.warn("ERROR: Antrag ist nicht abgelehn");
        }
        mv.setViewName("redirect:/anforderungen/" + userName);
        return mv;
    }
}
