package at.TARsystem.Spengergasse.service;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.LongTermAbsence;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.persistence.LongTermAbsenceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class LongTermAbsenceDomainService
{

    private final LongTermAbsenceRepository ltaRepo;

    @Transactional
    public Optional<LongTermAbsence> createSubstitution(@NotNull @Valid LongTermAbsence longTermAbsence)
    {
        return Optional.of(ltaRepo.save(longTermAbsence));
    }

    @Transactional
    public void sendToNextDecisionMaker(@NotNull Long mainRequestId)
    {
        List<LongTermAbsence> longTermAbsenceList = ltaRepo.findByMainRequestId(mainRequestId);
        AbsenceRequest mainRequest = longTermAbsenceList.get(0).getMainRequest();
        allDepartmentsAdministrators().forEach(departmentAdmin -> {
            Status status = Status
                    .of(mainRequest.getRDate(), mainRequest.getRequester(), UserRole.Lehrer, Decision.gestartet);
            status.setWaitFor(departmentAdmin);
            mainRequest.addStatus(status);
        });
        Status status1= Status.of(mainRequest.getRDate(), mainRequest.getRequester(), UserRole.Lehrer, Decision.gestartet);
        status1.setWaitFor(whoIsDirector());
        Status status2= Status.of(mainRequest.getRDate(), mainRequest.getRequester(), UserRole.Lehrer, Decision.gestartet);
        status2.setWaitFor(whoIsTimeTableAdmin());
        mainRequest.addStatuses(status1, status2);
        ltaRepo.save(longTermAbsenceList.get(0));
    }

    @Transactional
    public Optional<LongTermAbsence> setRealDuration(Optional<LongTermAbsence> existingLongTermAbsence, int realDuration) {
        if (existingLongTermAbsence.isPresent()) {
            LongTermAbsence longTermAbsence = existingLongTermAbsence.get();
            longTermAbsence.setRealDuration(realDuration);
            return Optional.of(ltaRepo.save(longTermAbsence));
        }
        return Optional.empty();
    }

    private List<String> allDepartmentsAdministrators()
    {
        return new ArrayList<>();
    }

    private String whoIsDirector()
    {
        return "";
    }

    private String whoIsTimeTableAdmin()
    {
        return "";
    }
}
