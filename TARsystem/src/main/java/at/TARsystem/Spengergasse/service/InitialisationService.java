package at.TARsystem.Spengergasse.service;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.LessonShift;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.persistence.LessonShiftRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InitialisationService
{
    private final LessonShiftRepository lsRepo;

    @PostConstruct
    protected void initDemoData() {
        log.info("------------ BEFORE INIT ------------");
        AbsenceRequest request1= AbsenceRequest
                .of("mos15809", LocalDateTime.now().minusDays(5), LocalDate.now().minusDays(3));
        request1.setReason("Verlegung");
        Status st1 = Status.of(request1.getRDate(), "mos15809", UserRole.Lehrer, Decision.gestartet);
        st1.setWaitFor("berger");
        request1.addStatus(st1);
        AbsenceRequest request2= AbsenceRequest
                .of("mos15809", LocalDateTime.now().minusDays(10), LocalDate.now().minusDays(8));
        request2.setReason("Verlegung");
        Status st2 = Status.of(request2.getRDate(), "mos15809", UserRole.Lehrer, Decision.gestartet);
        st2.setWaitFor("berger");
        Status st3 = Status.of(request2.getRDate().plusHours(2), "berger", UserRole.AV, Decision.akzeptiert);
        st3.setWaitFor("Zemanek");
        request2.addStatuses(st2,st3);
        LessonShift lessonShift1 = LessonShift
                .of(request1, "5ABIF", 5, 6, request1.getAbsenceDate().plusDays(7), 5, 6);
        LessonShift lessonShift2 = LessonShift
                .of(request2, "5ABIF", 3, 4, request2.getAbsenceDate().plusDays(10), 3, 4);

        lsRepo.save(Arrays.asList(lessonShift1,lessonShift2));
        log.info("------------ AFTER INIT ------------");
    }
}
