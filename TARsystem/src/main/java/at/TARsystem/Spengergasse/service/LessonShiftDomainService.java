package at.TARsystem.Spengergasse.service;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.LessonShift;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.persistence.LessonShiftRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class LessonShiftDomainService
{
    private final LessonShiftRepository lsRepo;
    private final AbsenceRequestDomainService abrService;

    @Transactional
    public Optional<LessonShift> createLessonShift(@NotNull @Valid LessonShift lessonShift)
    {
        return Optional.of(lsRepo.save(lessonShift));
    }

    @Transactional
    public String sendToNextDecisionMaker(Optional<LessonShift> existingLessonShift)
    {
        if (existingLessonShift.isPresent() && existingLessonShift.get().getId() != null)
        {
            AbsenceRequest mainRequest = existingLessonShift.get().getMainRequest();
            String className = existingLessonShift.get().getClassName();
            if (!mainRequest.isRequestWaitFor(whoIsDepartmentAdmin(className)))
            {
                Status st = mainRequest.getStatus_history().stream().filter(status -> status.getWhoTriggered()
                                                                                            .equalsIgnoreCase
                                                                                                    (whoIsDepartmentAdmin(className)))
                                       .findFirst().get();
                if (st.getDecision() == Decision.akzeptiert)
                {
                    if (st.getWaitFor() == null)
                    {
                        st.setWaitFor(whoIsDirector());
                        lsRepo.save(existingLessonShift.get());
                        return whoIsDirector();
                    }
                    else
                    {
                        if (!mainRequest.isRequestWaitFor(whoIsDirector()))
                        {
                            st = mainRequest.getStatus_history().stream().filter(status -> status.getWhoTriggered()
                                                                                                 .equalsIgnoreCase
                                                                                                         (whoIsDirector()))
                                            .findFirst().get();
                            if (st.getDecision() == Decision.akzeptiert)
                            {
                                if (st.getWaitFor() == null)
                                {
                                    st.setWaitFor(whoIsTimeTableAdmin());
                                    lsRepo.save(existingLessonShift.get());
                                    return whoIsTimeTableAdmin();
                                }
                                return "finished";
                            }
                            return "denied";
                        }
                        return whoIsDirector();
                    }
                }
                return "denied";
            }
            return whoIsDepartmentAdmin(className);
        }
        log.warn("Der Antrag existiert nicht in der Datenbank");
        return "ERROR";
    }

    @Transactional
    public String acceptOrDenySubstitution(Optional<LessonShift> existingLessonshift, @Valid @NotNull Status status)
    {
        String whoIsNext = "";
        if (existingLessonshift.isPresent() && existingLessonshift.get().getId() != null)
        {
            AbsenceRequest mainRequest = existingLessonshift.get().getMainRequest();
            Optional<AbsenceRequest> acceptedOrDeniedRequest = abrService
                    .acceptOrDenyRequest(Optional.of(mainRequest), status);

            if (acceptedOrDeniedRequest.isPresent())
            {
                if (status.getDecision() != Decision.abgelehnt)
                {
                    whoIsNext = sendToNextDecisionMaker(existingLessonshift);
                }
                else
                {
                    whoIsNext = "finished";
                }
            }
            else
            {
                whoIsNext = "ERROR";
            }
        }
        else
        {
            whoIsNext = "ERROR";
        }

        return whoIsNext;
    }

    public String whoIsDepartmentAdmin(String className)
    {
        return "berger";
    }

    public String whoIsDirector()
    {
        return "zemaneck";
    }

    public String whoIsTimeTableAdmin()
    {
        return "gruendorf";
    }
}
