package at.TARsystem.Spengergasse.service;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.Entities.Substitution;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.persistence.SubstitutionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class SubstitutionDomainService
{
    private final SubstitutionRepository subRepo;
    private final AbsenceRequestDomainService abrService;

    @Transactional
    public Optional<Substitution> createSubstitution(@NotNull @Valid Substitution substitution)
    {
        return Optional.of(subRepo.save(substitution));
    }

    @Transactional
    public String sendToNextDecisionMaker(Optional<Substitution> existingSubstitution)
    {
        if (existingSubstitution.isPresent() && existingSubstitution.get().getId() != null)
        {
            AbsenceRequest mainRequest = existingSubstitution.get().getMainRequest();
            List<Substitution> substitutionList = subRepo.findByMainRequestId(mainRequest.getId());
            boolean isWaitForSubstitutedTeachers = substitutionList.stream().allMatch(substitution -> mainRequest
                    .isRequestWaitFor(substitution.getSubstitutedTeacher()));
            String className = existingSubstitution.get().getClassName();
            String substitutedTeacher = existingSubstitution.get().getSubstitutedTeacher();

            if (!isWaitForSubstitutedTeachers)
            {
                Status st = mainRequest.getStatus_history().stream()
                                       .filter(status -> status.getWhoTriggered() == substitutedTeacher).findFirst()
                                       .get();
                if (st.getDecision() == Decision.akzeptiert)
                {
                    if (st.getWaitFor() == null)
                    {
                        st.setWaitFor(whoIsDepartmentAdmin(className));
                        subRepo.save(existingSubstitution.get());
                        return whoIsDepartmentAdmin(className);
                    }
                    else
                    {
                        if (!mainRequest.isRequestWaitFor(whoIsDepartmentAdmin(className)))
                        {
                            st = mainRequest.getStatus_history().stream()
                                            .filter(status -> status.getWhoTriggered() == whoIsDepartmentAdmin
                                                    (className))
                                            .findFirst().get();
                            if (st.getDecision() == Decision.akzeptiert)
                            {
                                if (st.getWaitFor() == null)
                                {
                                    st.setWaitFor(whoIsDirector());
                                    subRepo.save(existingSubstitution.get());
                                    return whoIsDirector();
                                }
                                else
                                {
                                    if (!mainRequest.isRequestWaitFor(whoIsDirector()))
                                    {
                                        st = mainRequest.getStatus_history().stream()
                                                        .filter(status -> status.getWhoTriggered() == whoIsDirector())
                                                        .findFirst().get();
                                        if (st.getDecision() == Decision.akzeptiert)
                                        {
                                            if (st.getWaitFor() == null)
                                            {
                                                st.setWaitFor(whoIsTimeTableAdmin());
                                                subRepo.save(existingSubstitution.get());
                                                return whoIsTimeTableAdmin();
                                            }
                                            return "finished";
                                        }
                                    }
                                    return whoIsDirector();
                                }
                            }
                            return null;
                        }
                        return whoIsDepartmentAdmin(className);
                    }
                }
                return null;
            }
            return substitutionList.stream().filter(substitution -> mainRequest
                    .isRequestWaitFor(substitution.getSubstitutedTeacher())).findFirst().get().getSubstitutedTeacher();
        }
        log.warn("Request doesn't exist in DB");
        return "ERROR";
    }

    @Transactional
    public Optional<Substitution> acceptOrDenySubstitution(Optional<Substitution> existingSubstitution,
                                                           @Valid @NotNull Status status)
    {
        if (existingSubstitution.isPresent() && existingSubstitution.get().getId() != null)
        {
            AbsenceRequest mainRequest = existingSubstitution.get().getMainRequest();
            Optional<AbsenceRequest> acceptedOrDeniedRequest = abrService
                    .acceptOrDenyRequest(Optional.of(mainRequest), status);
            if (acceptedOrDeniedRequest.isPresent())
            {
                String whoIsNext = sendToNextDecisionMaker(existingSubstitution);
                if (whoIsNext != "ERROR" || whoIsNext != null) return existingSubstitution;
            }
            log.warn("Some Problems while changing request status");
            return Optional.empty();
        }
        log.warn("Request doesn't exist in DB");
        return Optional.empty();
    }

    private String whoIsDepartmentAdmin(String className)
    {
        return "";
    }

    private String whoIsDirector()
    {
        return "";
    }

    private String whoIsTimeTableAdmin()
    {
        return "";
    }
}
