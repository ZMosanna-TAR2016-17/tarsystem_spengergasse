package at.TARsystem.Spengergasse.service;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.persistence.AbsenceRequestRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class AbsenceRequestDomainService
{
    private final AbsenceRequestRepository arRepo;

    @Transactional
    public Optional<AbsenceRequest> createAbsenceRequest(@NotNull @Valid AbsenceRequest absenceRequest)
    {
        return Optional.of(arRepo.save(absenceRequest));
    }

    @Transactional
    public Optional<AbsenceRequest> acceptOrDenyRequest(Optional<AbsenceRequest> existingAabsenceRequest,
                                                        @NotNull @Valid Status status)
    {
        if (existingAabsenceRequest.isPresent() && existingAabsenceRequest.get().getId() != null)
        {
            AbsenceRequest absenceRequest = existingAabsenceRequest.get();
            if (absenceRequest.getStatus_history().contains(status))
            {
                log.warn("You tried to add a status which already exists");
                return Optional.empty();
            }
            else
            {
                absenceRequest.addStatus(status);
                return Optional.of(arRepo.save(absenceRequest));
            }
        }
        log.warn("Request doesn't exist in DB");
        return Optional.empty();
    }
}
