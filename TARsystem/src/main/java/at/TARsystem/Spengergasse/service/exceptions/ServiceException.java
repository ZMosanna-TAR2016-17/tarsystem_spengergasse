package at.TARsystem.Spengergasse.service.exceptions;

public class ServiceException extends RuntimeException {

    private ServiceException(String message) {
        super(message);
    }

    private ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    // factory methods
    public static ServiceException forDuplicateName(String name) {
        return new DuplicateNameServiceException(String.format("Name %s is already existing!", name));
    }

    public static class DuplicateNameServiceException extends ServiceException {

        private DuplicateNameServiceException(String message) {
            super(message);
        }

        private DuplicateNameServiceException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
