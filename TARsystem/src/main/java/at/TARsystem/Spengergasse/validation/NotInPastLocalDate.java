package at.TARsystem.Spengergasse.validation;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.time.LocalDate;

@Constraint(validatedBy = NotInPastLocalDate.NotInPastValidator.class)
public @interface NotInPastLocalDate
{
    String message() default "{javax.validation.constraints.Past.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class NotInPastValidator implements ConstraintValidator< NotInPastLocalDate, LocalDate>
    {
        public void initialize(NotInPastLocalDate future)
        {

        }

        public boolean isValid(LocalDate localDate,ConstraintValidatorContext context)
        {
            return  localDate.isAfter(LocalDate.now());
        }
    }
}
