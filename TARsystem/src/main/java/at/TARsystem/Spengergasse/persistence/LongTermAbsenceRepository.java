package at.TARsystem.Spengergasse.persistence;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.LongTermAbsence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface LongTermAbsenceRepository extends JpaRepository<LongTermAbsence, Long>
{
    Optional<LongTermAbsence> findById(Long id);

    List<LongTermAbsence> findByMainRequest(AbsenceRequest mainRequest);
    List<LongTermAbsence> findByMainRequestAbsenceDate(LocalDate absenceDate);
    List<LongTermAbsence> findByMainRequestId(Long id);
    List<LongTermAbsence> findByMainRequestRequester(String requester);
}
