package at.TARsystem.Spengergasse.persistence;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.Substitution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface SubstitutionRepository extends JpaRepository<Substitution, Long>
{
    Optional<Substitution> findById(Long id);

    List<Substitution> findByMainRequest(AbsenceRequest mainRequest);
    List<Substitution> findByMainRequestAbsenceDate(LocalDate absenceDate);
    List<Substitution> findByMainRequestId(Long id);
    List<Substitution> findByMainRequestRequester(String requester);

    List<Substitution> findByClassName(String className);
    List<Substitution> findBySubstitutedTeacher(String substitutedTeacher);

    List<Substitution> findByMainRequestRDateAfterAndSubstitutedTeacher(LocalDateTime rDate ,String substitutedTeacher);
}
