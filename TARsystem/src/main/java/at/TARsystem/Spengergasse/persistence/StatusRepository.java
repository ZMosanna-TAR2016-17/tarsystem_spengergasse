package at.TARsystem.Spengergasse.persistence;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long>
{
    Optional<Status> findById(Long id);

    List<Status> findByRequest(AbsenceRequest request);
    List<Status> findByRequestAbsenceDate(LocalDate absenceDate);
    List<Status> findByRequestId(Long id);
    List<Status> findByRequestRequester(String requester);

    List<Status> findByTriggerTS(LocalDateTime triggerTS);

    List<Status> findByWhoTriggered(String whoTriggered);
    List<Status> findByWhoTriggeredAndRequestRDateAfter(String whoTriggered,LocalDateTime rDate);
    List<Status> findByWhoTriggeredAndRequestRDateBefore(String whoTriggered,LocalDateTime rDate);
    List<Status> findByWhoTriggeredAndRequestRDateBetween(String whoTriggered,LocalDateTime rDate1,LocalDateTime rDate2);

    @Query(" select s from Status s " +
           " where s.triggerTS=(select MAX(ss.triggerTS) from Status ss group by ss.request having s.request = ss.request) " +
           " AND s.waitFor= :waitFor")
    List<Status> findByLastStatusWaitFor(@Param("waitFor")String waitFor);
}
