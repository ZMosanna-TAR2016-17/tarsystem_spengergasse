package at.TARsystem.Spengergasse.persistence;


import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.LessonShift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@Repository
public interface LessonShiftRepository extends JpaRepository<LessonShift, Long>
{
    Optional<LessonShift> findById(Long id);

    List<LessonShift> findByMainRequest(AbsenceRequest mainRequest);
    List<LessonShift> findByMainRequestAbsenceDate(LocalDate absenceDate);
    List<LessonShift> findByMainRequestId(Long id);
    List<LessonShift> findByMainRequestRequester(String requester);

    List<LessonShift> findByClassName(String className);
    List<LessonShift> findByMainRequestRequesterAndClassName(String requester,String className);
    List<LessonShift> findByReturnDate(LocalDate returnDate);


}
