package at.TARsystem.Spengergasse.persistence;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AbsenceRequestRepository extends JpaRepository<AbsenceRequest, Long>
{
    Optional<AbsenceRequest> findById(Long id);
    List<AbsenceRequest> findByRDate(LocalDateTime rDate);
    List<AbsenceRequest> findByRequester(String requester);

    List<AbsenceRequest> findByRequesterAndAbsenceDate(String requester, LocalDate absenceDate);
    List<AbsenceRequest> findByRequesterAndRDateAfter(String requester, LocalDateTime rDate);
    List<AbsenceRequest> findByRequesterAndRDateBefore(String requester, LocalDateTime rDate);
    List<AbsenceRequest> findByRequesterAndRDateBetween(String requester, LocalDateTime rDate1, LocalDateTime rDate2);
    @Query(" SELECT ar FROM AbsenceRequest ar JOIN ar.status_history sh " +
           " WHERE ar.requester=:username and " +
           "       sh.tRole=:userRole and " +
           " sh.decision=:desition")
    List<AbsenceRequest> findByRequesterAndStatusTRoleAndStatusDesition(@Param("username") String requester,
                                                                        @Param("userRole") UserRole tRole,
                                                                        @Param("desition") Decision desition);
    @Query(" SELECT ar FROM AbsenceRequest ar JOIN ar.status_history sh " +
           " where sh.triggerTS = (select max(sh.triggerTS) from sh) and sh.waitFor = :waitFor")
    List<AbsenceRequest> findByLastStatusWaitFor(@Param("waitFor") String waitFor);
    @Query(" SELECT ar FROM AbsenceRequest ar JOIN ar.status_history sh WHERE sh.whoTriggered = :whoTriggered")
    List<AbsenceRequest> findByStatusWhoTriggered(@Param("whoTriggered") String whoTriggered);
}
