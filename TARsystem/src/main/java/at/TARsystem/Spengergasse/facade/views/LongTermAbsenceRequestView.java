package at.TARsystem.Spengergasse.facade.views;

import at.TARsystem.Spengergasse.facade.views.subviews.AbsenceRequestView;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor(staticName = "of")
@Builder
public class LongTermAbsenceRequestView
{
    private final boolean result;
    private final Long id;
    private final AbsenceRequestView mainRequest;
    private final int estimDuration;
}
