package at.TARsystem.Spengergasse.facade.views.subviews;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor(staticName = "of")
@Builder
public class SubstitutionSubRequestView
{
    private final boolean result;
    private final Long id;
    private final String className;
    private final int absentFrom;
    private final int absentTo;
    private final String substitutedTeacher;
}
