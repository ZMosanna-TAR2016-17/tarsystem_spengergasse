package at.TARsystem.Spengergasse.facade.commands;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class AcceptOrDenyCommand
{
    @NonNull
    @NotNull
    private Long requestId;

    @NonNull
    @NotNull
    private CreateStatusCommand statusCommand;

}
