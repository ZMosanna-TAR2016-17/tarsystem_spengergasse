package at.TARsystem.Spengergasse.facade.commands;

import at.TARsystem.Spengergasse.validation.NotInPastLocalDate;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class CreateAbsenceRequestCommand
{
    @NotNull
    @NonNull
    private String requester;

    @NonNull
    @NotNull
    private LocalDateTime rDate;

    @NonNull
    @NotNull
    @NotInPastLocalDate
    private LocalDate absenceDate;

    @Getter
    @Setter
    private String reason;
}
