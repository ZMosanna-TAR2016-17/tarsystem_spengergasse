package at.TARsystem.Spengergasse.facade;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.Substitution;
import at.TARsystem.Spengergasse.facade.commands.CreateSubstitutionRequestCommand;
import at.TARsystem.Spengergasse.facade.views.SubstitutionRequestView;
import at.TARsystem.Spengergasse.facade.views.subviews.AbsenceRequestView;
import at.TARsystem.Spengergasse.facade.views.subviews.SubstitutionSubRequestView;
import at.TARsystem.Spengergasse.persistence.SubstitutionRepository;
import at.TARsystem.Spengergasse.service.SubstitutionDomainService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class SubstitutionFacade
{
    private final SubstitutionRepository subRepo;
    private final SubstitutionDomainService subDomainService;
    private final AbsenceRequestFacade arFacade;

    @Setter
    @Getter
    private List<CreateSubstitutionRequestCommand> tempSubstitutionCommandList = new ArrayList<>();

    @Setter
    @Getter
    private SubstitutionRequestView substitutionRequestView;

    public SubstitutionSubRequestView createSubstitution(@NotNull @Valid CreateSubstitutionRequestCommand command)
    {
        try
        {
            if(arFacade.getMainRequest().isPresent())
            {
                AbsenceRequest mainRequest = arFacade.getMainRequest().get();
                Optional<Substitution> createSubstitution = subDomainService.createSubstitution(Substitution
                                                                                                        .of(mainRequest,
                                                                                                            command.getClassName(),
                                                                                                            command.getAbsentFrom(),
                                                                                                            command.getAbsentTo(),
                                                                                                            command.getSubstitutedTeacher()));

                return createSubstitutionSubView(createSubstitution);
            }
            log.warn("Enige Problem waehrende Erstellen der Stundenstauch Antrag  ");
            return errorView();
        }
        catch (Exception ex)
        {
            log.warn("Enige Problem waehrende ein Stundenstauch Antrag Erstellen ", ex);
            return errorView();
        }
    }

    public SubstitutionRequestView saveSubstitutions()
    {
        try
        {
            substitutionRequestView= SubstitutionRequestView.of(Boolean.TRUE,
                                                              arFacade.createAbsenceRequestView(arFacade.getMainRequest()),
                                                              new ArrayList<>());
            tempSubstitutionCommandList.forEach(command -> {
                substitutionRequestView.getSubstitutionViwes().add(createSubstitution(command));
            });
            return substitutionRequestView;
        }
        catch (Exception ex)
        {
            log.warn("Enige Problem waehrende ein Stundenstauch Antrag Erstellen ", ex);
            AbsenceRequest mainRequest = AbsenceRequest.of("unbekannt", LocalDateTime.now(), LocalDate.now());
            AbsenceRequestView mainRequestView = arFacade.createAbsenceRequestView(Optional.of(mainRequest));
            return SubstitutionRequestView.of(Boolean.FALSE, mainRequestView, new ArrayList<>());
        }
    }

    public void addTempSubstitutionRequest(@NotNull @Valid CreateSubstitutionRequestCommand command)
    {
        tempSubstitutionCommandList.add(command);
    }

    public void resetTempLessonShiftRequestList()
    {
        tempSubstitutionCommandList.clear();
        substitutionRequestView=null;
    }

    private SubstitutionSubRequestView createSubstitutionSubView(Optional<Substitution> substitution)
    {
        return SubstitutionSubRequestView
                .builder()
                .result(substitution.isPresent() ? Boolean.TRUE : Boolean.FALSE)
                .id(substitution.isPresent() ? substitution.get().getId() : -1L)
                .className(substitution.isPresent() ? substitution.get().getClassName() : "unbekannt")
                .absentFrom(substitution.isPresent() ? substitution.get().getAbsentFrom() : -1)
                .absentTo(substitution.isPresent() ? substitution.get().getAbsentTo() : -1)
                .substitutedTeacher(substitution.isPresent() ? substitution.get().getSubstitutedTeacher() : "unbekannt")
                .build();
    }

    //Returns an invalid view
    private SubstitutionSubRequestView errorView()
    {
        AbsenceRequest mainRequest = AbsenceRequest.of("unbekannt", LocalDateTime.now(), LocalDate.now());
        return SubstitutionSubRequestView.of(Boolean.FALSE, -1L, "unbekannt", -1, -1,"unbekannt" );
    }
}
