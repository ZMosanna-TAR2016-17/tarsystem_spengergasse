package at.TARsystem.Spengergasse.facade.views.subviews;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@RequiredArgsConstructor(staticName = "of")
@Builder
public class AbsenceRequestView
{
    private final boolean result;
    private final Long id;
    private final String requester;
    private final String rDate;
    private final LocalDate absenceDate;
    private final String reason;
    private final List<StatusView> status_history;
    private final String statusToString;
}
