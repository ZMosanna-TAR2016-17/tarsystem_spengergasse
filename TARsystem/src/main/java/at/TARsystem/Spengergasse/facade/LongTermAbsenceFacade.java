package at.TARsystem.Spengergasse.facade;

import at.TARsystem.Spengergasse.persistence.LongTermAbsenceRepository;
import at.TARsystem.Spengergasse.service.LongTermAbsenceDomainService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class LongTermAbsenceFacade
{
    private final AbsenceRequestFacade arFacade;
    private final LongTermAbsenceRepository ltaRepo;
    private final LongTermAbsenceDomainService ltaDomainService;


}
