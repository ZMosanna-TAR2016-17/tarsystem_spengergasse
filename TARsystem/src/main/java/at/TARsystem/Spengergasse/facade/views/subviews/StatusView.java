package at.TARsystem.Spengergasse.facade.views.subviews;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;
@Data
@RequiredArgsConstructor(staticName = "of")
@Builder
public class StatusView
{
    private final boolean result;
    private final Long id;
    private final AbsenceRequest request;
    private final LocalDateTime triggerTS;
    private final String whoTriggered;
    private final UserRole tRole;
    private final Decision decision;
}
