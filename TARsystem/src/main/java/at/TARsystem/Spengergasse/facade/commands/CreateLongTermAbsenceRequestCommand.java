package at.TARsystem.Spengergasse.facade.commands;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class CreateLongTermAbsenceRequestCommand
{
    @NonNull
    @NotNull
    @Min(3)
    private int estimDuration;
}
