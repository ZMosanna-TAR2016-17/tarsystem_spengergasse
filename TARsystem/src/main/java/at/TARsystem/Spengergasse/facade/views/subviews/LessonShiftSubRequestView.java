package at.TARsystem.Spengergasse.facade.views.subviews;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Data
@RequiredArgsConstructor(staticName = "of")
@Builder
public class LessonShiftSubRequestView
{
    private final boolean result;
    private final Long id;
    private final String className;
    private final int absentFrom;
    private final int absentTo;
    private final LocalDate returnDate;
    private final int returnHourFrom;
    private final int returnHourTo;
}
