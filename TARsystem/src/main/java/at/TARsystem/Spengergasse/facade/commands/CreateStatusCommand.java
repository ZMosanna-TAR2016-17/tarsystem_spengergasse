package at.TARsystem.Spengergasse.facade.commands;

import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class CreateStatusCommand
{
    @NonNull
    @NotNull
    private LocalDateTime triggerTS;

    @Setter
    @NonNull
    @NotNull
    private String whoTriggered;

    @NonNull
    @NotNull
    @Enumerated(EnumType.STRING)
    private UserRole tRole;

    @NonNull
    @NotNull
    @Enumerated(EnumType.STRING)
    private Decision decision;
}
