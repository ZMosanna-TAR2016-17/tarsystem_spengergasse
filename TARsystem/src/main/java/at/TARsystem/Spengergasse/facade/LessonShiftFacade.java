package at.TARsystem.Spengergasse.facade;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.LessonShift;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.domain.NonEntities.CourseHour;
import at.TARsystem.Spengergasse.facade.commands.AcceptOrDenyCommand;
import at.TARsystem.Spengergasse.facade.commands.CreateAbsenceRequestCommand;
import at.TARsystem.Spengergasse.facade.commands.CreateLessonShiftRequestCommand;
import at.TARsystem.Spengergasse.facade.views.LessonShiftRequestView;
import at.TARsystem.Spengergasse.facade.views.subviews.AbsenceRequestView;
import at.TARsystem.Spengergasse.facade.views.subviews.LessonShiftSubRequestView;
import at.TARsystem.Spengergasse.persistence.LessonShiftRepository;
import at.TARsystem.Spengergasse.service.LessonShiftDomainService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class LessonShiftFacade
{
    private final LessonShiftRepository lsRepo;
    private final LessonShiftDomainService lsDomainService;
    private final AbsenceRequestFacade arFacade;

    @Setter
    @Getter
    private CreateAbsenceRequestCommand arCreateCommand;

    public void setMainRequest()
    {
        arFacade.setMainAbsenceRequest(arCreateCommand);
    }

    public LessonShiftSubRequestView createLessonShift(@NotNull @Valid CreateLessonShiftRequestCommand command)
    {
        try
        {
            if (arFacade.getMainRequest().isPresent())
            {
                AbsenceRequest mainRequest = arFacade.getMainRequest().get();
                Status status = Status.of(arCreateCommand.getRDate(), arCreateCommand
                        .getRequester(), UserRole.Lehrer, Decision.gestartet);
                status.setWaitFor(whoIsDepartmentAdmin(command.getClassName()));
                mainRequest.addStatus(status);
                Optional<LessonShift> createLessonShift = lsDomainService.createLessonShift(LessonShift
                                                                                                    .of(mainRequest,
                                                                                                        command
                                                                                                                .getClassName(), command
                                                                                                                .getAbsentFrom(), command
                                                                                                                .getAbsentTo(), command
                                                                                                                .getReturnDate(), command
                                                                                                                .getReturnHourFrom(), command
                                                                                                                .getReturnHourTo()));

                return createLessonShiftSubView(createLessonShift);
            }
            log.warn("Enige Problem waehrende Erstellen der Stundenstauch Antrag  ");
            return errorView();
        }
        catch (Exception ex)
        {
            log.warn("Enige Problem waehrende ein Stundenstauch Antrag Erstellen ", ex);
            return errorView();
        }
    }

    public LessonShiftRequestView saveLessonShift(@NotNull @Valid CreateLessonShiftRequestCommand command)
    {
        try
        {

            setMainRequest();
            return LessonShiftRequestView.of(Boolean.TRUE, arFacade
                    .createAbsenceRequestView(arFacade.getMainRequest()), createLessonShift(command));
        }
        catch (Exception ex)
        {
            log.warn("Enige Problem waehrende ein Stundenstauch Antrag Erstellen ", ex);
            AbsenceRequest mainRequest = AbsenceRequest.of("unbekannt", LocalDateTime.now(), LocalDate.now());
            AbsenceRequestView mainRequestView = arFacade.createAbsenceRequestView(Optional.of(mainRequest));
            return LessonShiftRequestView.of(Boolean.FALSE, mainRequestView, errorView());
        }
    }

    public List<LessonShiftRequestView> getAllLessonShiftsOfUser(String user)
    {
        List<AbsenceRequestView> absenceRequestViews = arFacade.getAllAbsenceRequestsOfUser(user);
        List<LessonShiftRequestView> lessonShiftRequestViews = new ArrayList<>();
        if (!absenceRequestViews.isEmpty())
        {
            absenceRequestViews.forEach(absenceRequestView -> {
                List<LessonShift> lessonShifts = lsRepo.findByMainRequestId(absenceRequestView.getId());
                if (!lessonShifts.isEmpty())
                {
                    LessonShiftSubRequestView lessonShiftSubRequestViews = lessonShifts.stream()
                                                                                       .map(lessonShift ->
                                                                                                    createLessonShiftSubView(Optional.of(lessonShift)))
                                                                                       .collect(Collectors.toList())
                                                                                       .get(0);
                    lessonShiftRequestViews
                            .add(LessonShiftRequestView.of(true, absenceRequestView, lessonShiftSubRequestViews));
                }
            });
        }
        return lessonShiftRequestViews;
    }

    public List<LessonShiftRequestView> getLatelyAppliedLessonShiftsOfUser(String user)
    {
        List<AbsenceRequestView> absenceRequestViews = arFacade.getLatelyAppliedAbsenceRequestsOfUser(user);
        List<LessonShiftRequestView> lessonShiftRequestViews = new ArrayList<>();
        if (!absenceRequestViews.isEmpty())
        {
            absenceRequestViews.forEach(absenceRequestView -> {
                List<LessonShift> lessonShifts = lsRepo.findByMainRequestId(absenceRequestView.getId());
                if (!lessonShifts.isEmpty())
                {
                    LessonShiftSubRequestView lessonShiftSubRequestViews = lessonShifts.stream()
                                                                                       .map(lessonShift ->
                                                                                                    createLessonShiftSubView(Optional.of(lessonShift)))
                                                                                       .collect(Collectors.toList())
                                                                                       .get(0);
                    lessonShiftRequestViews
                            .add(LessonShiftRequestView.of(true, absenceRequestView, lessonShiftSubRequestViews));
                }
            });
        }
        return lessonShiftRequestViews;
    }

    public String acceptOrDenyLessonShift(@NotNull @Valid AcceptOrDenyCommand command)
    {
        List<LessonShift> lessonShifts = lsRepo.findByMainRequestId(command.getRequestId());
        if (!lessonShifts.isEmpty())
        {
            Status status = Status
                    .of(command.getStatusCommand().getTriggerTS(), command.getStatusCommand().getWhoTriggered(), command
                            .getStatusCommand().getTRole(), command.getStatusCommand().getDecision());

            String whoIsNext = lsDomainService
                    .acceptOrDenySubstitution(Optional.of(lessonShifts.get(0)), status);
            return whoIsNext;
        }
        return "Antrag ist nicht vorhand" ;
    }

    public List<LessonShiftRequestView> getAllLessonShiftsWaitForUser(String user)
    {
        List<AbsenceRequestView> absenceRequestViews = arFacade.getAllAbsenceRequestsWaitForUser(user);
        List<LessonShiftRequestView> lessonShiftRequestViews = new ArrayList<>();
        if (!absenceRequestViews.isEmpty())
        {
            absenceRequestViews.forEach(absenceRequestView -> {
                List<LessonShift> lessonShifts = lsRepo.findByMainRequestId(absenceRequestView.getId());
                if (!lessonShifts.isEmpty())
                {
                    LessonShiftSubRequestView lessonShiftSubRequestViews = lessonShifts.stream()
                                                                                       .map(lessonShift ->
                                                                                                    createLessonShiftSubView(Optional.of(lessonShift)))
                                                                                       .collect(Collectors.toList())
                                                                                       .get(0);
                    lessonShiftRequestViews
                            .add(LessonShiftRequestView.of(true, absenceRequestView, lessonShiftSubRequestViews));
                }
            });
        }
        return lessonShiftRequestViews;
    }

    private LessonShiftSubRequestView createLessonShiftSubView(Optional<LessonShift> lessonShift)
    {
        return LessonShiftSubRequestView.builder().result(lessonShift.isPresent() ? Boolean.TRUE : Boolean.FALSE)
                                        .id(lessonShift.isPresent() ? lessonShift.get().getId() : -1L)
                                        .className(lessonShift.isPresent() ? lessonShift.get()
                                                                                        .getClassName() : "unbekannt")
                                        .absentFrom(lessonShift.isPresent() ? lessonShift.get().getAbsentFrom() : -1)
                                        .absentTo(lessonShift.isPresent() ? lessonShift.get().getAbsentTo() : -1)
                                        .returnDate(lessonShift.isPresent() ? lessonShift.get()
                                                                                         .getReturnDate() : LocalDate
                                                .now()).returnHourFrom(lessonShift.isPresent() ? lessonShift.get()
                                                                                                            .getReturnHourFrom() : -1)
                                        .returnHourTo(lessonShift.isPresent() ? lessonShift.get()
                                                                                           .getReturnHourTo() : -1)
                                        .build();
    }

    //Returns an invalid view
    private LessonShiftSubRequestView errorView()
    {
        return LessonShiftSubRequestView.of(Boolean.FALSE, -1L, "unbekannt", -1, -1, LocalDate.now(), -1, -1);
    }

    public List<String> allSchoolClasses()
    {
        return Arrays
                .asList("1ABIF", "2ABIF", "3ABIF", "4ABIF", "5ABIF", "6ABIF", "1BBIF", "2BBIF", "3BBIF", "4BBIF", "5BBIF", "6BBIF");
    }

    public List<CourseHour> getCoursHours()
    {
        return Arrays.asList(CourseHour.of(1, LocalTime.of(8, 0),50),
                             CourseHour.of(2, LocalTime.of(8,50),50),
                             CourseHour.of(3, LocalTime.of(9,55),50),
                             CourseHour.of(4, LocalTime.of(10,45),50),
                             CourseHour.of(5, LocalTime.of(11,45),50),
                             CourseHour.of(6, LocalTime.of(12,35),50),
                             CourseHour.of(7, LocalTime.of(13,25),50),
                             CourseHour.of(8, LocalTime.of(14,25),50),
                             CourseHour.of(9, LocalTime.of(15,15),50),
                             CourseHour.of(10,LocalTime.of(16,15),50),
                             CourseHour.of(11,LocalTime.of(17,10),45),
                             CourseHour.of(12,LocalTime.of(17,55),45),
                             CourseHour.of(13,LocalTime.of(18,50),45),
                             CourseHour.of(14,LocalTime.of(19,35),45),
                             CourseHour.of(15,LocalTime.of(20,30),45),
                             CourseHour.of(16,LocalTime.of(21,15),45));
    }

    public String whoIsDepartmentAdmin(String className)
    {
        return "berger";
    }

    public String whoIsDirector()
    {
        return "Zemaneck";
    }

    public String whoIsTimeTableAdmin()
    {
        return "gruendorf";
    }
}