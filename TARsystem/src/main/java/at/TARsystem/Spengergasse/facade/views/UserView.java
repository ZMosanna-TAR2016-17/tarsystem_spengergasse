package at.TARsystem.Spengergasse.facade.views;

import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor(staticName = "of")
@Builder
public class UserView
{
    private final String username;
    private final String email;
    private final String firstName;
    private final String lastName;
    private final String shortId;
    private final UserRole userRole;
}
