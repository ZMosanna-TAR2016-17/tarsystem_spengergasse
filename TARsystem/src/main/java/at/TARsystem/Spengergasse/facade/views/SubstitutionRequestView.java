package at.TARsystem.Spengergasse.facade.views;

import at.TARsystem.Spengergasse.facade.views.subviews.AbsenceRequestView;
import at.TARsystem.Spengergasse.facade.views.subviews.SubstitutionSubRequestView;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor(staticName = "of")
@Builder
public class SubstitutionRequestView
{
    private final boolean result;
    private final AbsenceRequestView mainRequest;
    private final List<SubstitutionSubRequestView> substitutionViwes;
}
