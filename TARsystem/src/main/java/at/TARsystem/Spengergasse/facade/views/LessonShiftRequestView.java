package at.TARsystem.Spengergasse.facade.views;

import at.TARsystem.Spengergasse.facade.views.subviews.AbsenceRequestView;
import at.TARsystem.Spengergasse.facade.views.subviews.LessonShiftSubRequestView;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor(staticName = "of")
@Builder
public class LessonShiftRequestView
{
    private final boolean result;
    private final AbsenceRequestView mainRequest;
    private final LessonShiftSubRequestView lessonShiftViwe;
}
