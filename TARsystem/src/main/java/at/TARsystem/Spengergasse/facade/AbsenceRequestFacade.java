package at.TARsystem.Spengergasse.facade;

import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.facade.commands.CreateAbsenceRequestCommand;
import at.TARsystem.Spengergasse.facade.views.subviews.AbsenceRequestView;
import at.TARsystem.Spengergasse.facade.views.subviews.StatusView;
import at.TARsystem.Spengergasse.persistence.AbsenceRequestRepository;
import at.TARsystem.Spengergasse.service.AbsenceRequestDomainService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class AbsenceRequestFacade
{

    private final AbsenceRequestDomainService arDomainService;
    private final AbsenceRequestRepository arRepo;
    @Getter
    private Optional<AbsenceRequest> mainRequest;

    public void setMainAbsenceRequest(@NotNull @Valid CreateAbsenceRequestCommand arCreateCommand)
    {
        AbsenceRequest absenceRequest = AbsenceRequest
                .of(arCreateCommand.getRequester(), arCreateCommand.getRDate(), arCreateCommand.getAbsenceDate());
        absenceRequest.setReason(arCreateCommand.getReason());
        mainRequest = arDomainService.createAbsenceRequest(absenceRequest);
    }

    public AbsenceRequestView createAbsenceRequestView(Optional<AbsenceRequest> absenceRequest)
    {
        Boolean isPresent = absenceRequest.isPresent();
        List<StatusView> statusViewList = new ArrayList<>();
        StringBuilder sb = new StringBuilder("");
        if (isPresent)
        {
            absenceRequest.get().getStatus_history().forEach(status -> {
                if (status.getDecision() != Decision.gestartet)
                {
                    statusViewList.add(StatusView.builder().triggerTS(status.getTriggerTS())
                                                 .whoTriggered(status.getWhoTriggered()).tRole(status.getTRole())
                                                 .decision(status.getDecision()).build());
                }
            });

            if (!statusViewList.isEmpty())
            {
                statusViewList.forEach(statusView -> {
                    sb.append(statusView.getTRole()).append(" ")
                      .append(statusView.getWhoTriggered()).append(" ").append(statusView.getDecision()).append("\n");
                });
            }
            else sb.append("pending");
        }
        return AbsenceRequestView.builder().result(isPresent ? Boolean.TRUE : Boolean.FALSE)
                                 .id(isPresent ? absenceRequest.get().getId() : -1L)
                                 .requester(isPresent ? absenceRequest.get().getRequester() : "unbekannt")
                                 .rDate(isPresent ? absenceRequest.get().getRDate().format(DateTimeFormatter
                                                                                                   .ofPattern("yyyy-MM-dd HH:mm")) : LocalDateTime
                                         .now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")))
                                 .absenceDate(isPresent ? absenceRequest.get().getAbsenceDate() : LocalDate.now())
                                 .reason(isPresent ? absenceRequest.get().getReason() : "")
                                 .status_history(statusViewList).statusToString(sb.toString()).build();
    }

    public AbsenceRequestView getAbsenceRequest(Long id)
    {
        Optional<AbsenceRequest> absenceRequest = arRepo.findById(id);
        List<StatusView> statusViewList = new ArrayList<>();
        StringBuilder sb = new StringBuilder("");
        if (absenceRequest.isPresent())
        {
            absenceRequest.get().getStatus_history().forEach(status -> {
                if (status.getDecision() != Decision.gestartet)
                {
                    statusViewList.add(StatusView.builder().triggerTS(status.getTriggerTS())
                                                 .whoTriggered(status.getWhoTriggered()).tRole(status.getTRole())
                                                 .decision(status.getDecision()).build());
                }
            });

            if (!statusViewList.isEmpty())
            {
                statusViewList.forEach(statusView -> {
                    sb.append(statusView.getTRole()).append(" ")
                      .append(statusView.getWhoTriggered()).append(" ").append(statusView.getDecision()).append("\n");
                });
            }
            else sb.append("pending");
        }
        return (absenceRequest.isPresent()) ? AbsenceRequestView.builder().result(true).id(absenceRequest.get().getId())
                                                                .requester(absenceRequest.get().getRequester())
                                                                .rDate(absenceRequest.get().getRDate()
                                                                                     .format(DateTimeFormatter
                                                                                                     .ofPattern
                                                                                                             ("yyyy-MM-dd HH:mm")))
                                                                .absenceDate(absenceRequest.get().getAbsenceDate())
                                                                .reason(absenceRequest.get().getReason())
                                                                .status_history(statusViewList)
                                                                .statusToString(sb.toString())
                                                                .build() : AbsenceRequestView
                .of(false, -1L, "unbekannt", LocalDateTime.now().format(DateTimeFormatter
                                                                                .ofPattern("yyyy-MM-dd HH:mm")),
                    LocalDate
                            .now(), "", new ArrayList<>(), "");
    }

    public List<AbsenceRequestView> getAllAbsenceRequestsOfUser(String user)
    {
        List<AbsenceRequest> absenceRequests = arRepo.findByRequester(user);
        List<AbsenceRequestView> absenceRequestViews = new ArrayList<>();
        if (!absenceRequests.isEmpty())
        {
            absenceRequests.sort(Comparator.comparing(AbsenceRequest::getRDate).reversed());
            absenceRequests.forEach(absenceRequest -> {
                final List<StatusView>[] statusViewList = new ArrayList[1];
                statusViewList[0] = new ArrayList<>();
                absenceRequest.getStatus_history().forEach(status -> {

                    if (status.getDecision() != Decision.gestartet)
                    {
                        statusViewList[0].add(StatusView.builder().result(true).triggerTS(status.getTriggerTS())
                                                        .whoTriggered(status.getWhoTriggered()).tRole(status.getTRole())
                                                        .decision(status.getDecision()).build());
                    }
                });
                StringBuilder sb = new StringBuilder("");
                if (!statusViewList[0].isEmpty())
                {
                    statusViewList[0].forEach(statusView -> {
                        sb.append(statusView.getTRole()).append(" ")
                          .append(statusView.getWhoTriggered()).append(" ").append(statusView.getDecision())
                          .append("\n");
                    });
                }
                else sb.append("pending");
                absenceRequestViews.add(AbsenceRequestView.builder().result(true).id(absenceRequest.getId())
                                                          .requester(absenceRequest.getRequester())
                                                          .rDate(absenceRequest.getRDate().format(DateTimeFormatter
                                                                                                          .ofPattern
                                                                                                                  ("yyyy-MM-dd HH:mm")))
                                                          .absenceDate(absenceRequest.getAbsenceDate())
                                                          .reason(absenceRequest.getReason())
                                                          .status_history(statusViewList[0])
                                                          .statusToString(sb.toString()).build());
            });
        }

        return absenceRequestViews;
    }

    public List<AbsenceRequestView> getLatelyAppliedAbsenceRequestsOfUser(String user)
    {
        List<AbsenceRequest> absenceRequests = arRepo
                .findByRequesterAndRDateAfter(user, LocalDateTime.now().minusMonths(1));
        List<AbsenceRequestView> absenceRequestViews = new ArrayList<>();
        if (!absenceRequests.isEmpty())
        {
            absenceRequests.sort(Comparator.comparing(AbsenceRequest::getRDate).reversed());
            absenceRequests.forEach(absenceRequest -> {
                final List<StatusView>[] statusViewList = new ArrayList[1];
                statusViewList[0] = new ArrayList<>();
                absenceRequest.getStatus_history().forEach(status -> {

                    if (status.getDecision() != Decision.gestartet)
                    {
                        statusViewList[0].add(StatusView.builder().triggerTS(status.getTriggerTS())
                                                        .whoTriggered(status.getWhoTriggered()).tRole(status.getTRole())
                                                        .decision(status.getDecision()).build());
                    }
                });
                StringBuilder sb = new StringBuilder("");
                if (!statusViewList[0].isEmpty())
                {
                    statusViewList[0].forEach(statusView -> {
                        sb.append(statusView.getTRole()).append(" ")
                          .append(statusView.getWhoTriggered()).append(" ").append(statusView.getDecision())
                          .append("\n");
                    });
                }
                else sb.append("pending");
                absenceRequestViews.add(AbsenceRequestView.builder().result(true).id(absenceRequest.getId())
                                                          .requester(absenceRequest.getRequester())
                                                          .rDate(absenceRequest.getRDate().format(DateTimeFormatter
                                                                                                          .ofPattern
                                                                                                                  ("yyyy-MM-dd HH:mm")))
                                                          .absenceDate(absenceRequest.getAbsenceDate())
                                                          .reason(absenceRequest.getReason())
                                                          .status_history(statusViewList[0])
                                                          .statusToString(sb.toString()).build());
            });
        }
        return absenceRequestViews;
    }

    public List<AbsenceRequestView> getAllAbsenceRequestsWaitForUser(String user)
    {
        List<AbsenceRequest> absenceRequests = arRepo.findAll().stream()
                                                     .filter(absenceRequest -> absenceRequest.isRequestWaitFor(user))
                                                     .collect(Collectors.toList());

        List<AbsenceRequestView> absenceRequestViews = new ArrayList<>();
        if (!absenceRequests.isEmpty())
        {
            absenceRequests.sort(Comparator.comparing(AbsenceRequest::getRDate).reversed());
            absenceRequests.forEach(absenceRequest -> {
                final List<StatusView>[] statusViewList = new ArrayList[1];
                statusViewList[0] = new ArrayList<>();
                absenceRequest.getStatus_history().forEach(status -> {
                    if (status.getDecision() != Decision.gestartet)
                    {
                        statusViewList[0].add(StatusView.builder().triggerTS(status.getTriggerTS())
                                                        .whoTriggered(status.getWhoTriggered()).tRole(status.getTRole())
                                                        .decision(status.getDecision()).build());
                    }
                });
                StringBuilder sb = new StringBuilder("");
                if (!statusViewList[0].isEmpty())
                {
                    statusViewList[0].forEach(statusView -> {
                        sb.append(statusView.getTRole()).append(" ")
                          .append(statusView.getWhoTriggered()).append(" ").append(statusView.getDecision())
                          .append("\n");
                    });
                }
                else sb.append("pending");
                absenceRequestViews.add(AbsenceRequestView.builder().result(true).id(absenceRequest.getId())
                                                          .requester(absenceRequest.getRequester())
                                                          .rDate(absenceRequest.getRDate().format(DateTimeFormatter
                                                                                                          .ofPattern
                                                                                                                  ("yyyy-MM-dd HH:mm")))
                                                          .absenceDate(absenceRequest.getAbsenceDate())
                                                          .reason(absenceRequest.getReason())
                                                          .status_history(statusViewList[0])
                                                          .statusToString(sb.toString()).build());
            });
        }
        return absenceRequestViews;
    }
}
