package at.TARsystem.Spengergasse.facade.commands;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class CreateSubstitutionRequestCommand
{
    @NonNull
    @NotNull
    private String className;

    @NonNull
    @NotNull
    private int absentFrom;

    @NonNull
    @NotNull
    private int absentTo;

    @NonNull
    @NotNull
    private String substitutedTeacher;
}
