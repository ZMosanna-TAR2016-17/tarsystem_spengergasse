package at.TARsystem.Spengergasse.facade.commands;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class CreateLessonShiftRequestCommand
{
    @NonNull
    @NotNull
    private String className;

    @NonNull
    @NotNull
    private int absentFrom;

    @NonNull
    @NotNull
    private int absentTo;

    @NonNull
    @NotNull
    private LocalDate returnDate;

    @NonNull
    @NotNull
    private int returnHourFrom;

    @NonNull
    @NotNull
    private int returnHourTo;
}
