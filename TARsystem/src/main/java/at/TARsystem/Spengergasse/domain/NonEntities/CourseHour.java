package at.TARsystem.Spengergasse.domain.NonEntities;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(staticName = "of")
public class CourseHour
{
    @NonNull
    @NotNull
    //hour Identifire.for a certain period of  day time, it is the same every day.
    // For example: every day from 8:00 to 8:50 am is always identified by 1
    private int id;

    @NonNull
    @NotNull
    private LocalTime start;

    @NonNull
    @NotNull
    //shoud be in minutes
    private int duration;
}