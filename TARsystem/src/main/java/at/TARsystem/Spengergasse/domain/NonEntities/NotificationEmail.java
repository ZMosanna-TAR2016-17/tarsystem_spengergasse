package at.TARsystem.Spengergasse.domain.NonEntities;


import lombok.*;
import org.hibernate.validator.constraints.Email;

import javax.swing.text.html.HTML;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(staticName = "of")
public class NotificationEmail
{
    @NotNull
    @NonNull
    @Email
    private String from;

    @NotNull
    @NonNull
    @Email
    private String to;

    private String subject;

    private HTML body;
}
