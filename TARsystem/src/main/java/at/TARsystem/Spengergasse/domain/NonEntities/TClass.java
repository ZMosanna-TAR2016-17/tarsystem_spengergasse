package at.TARsystem.Spengergasse.domain.NonEntities;

import at.TARsystem.Spengergasse.domain.EnumTypes.ClassType;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(staticName = "of")
public class TClass
{

    @NonNull
    @NotNull
    private String name;

    @NonNull
    @NotNull
    @Enumerated(EnumType.STRING)
    private ClassType cType;

    @NonNull
    @NotNull
    private User DA;
}


