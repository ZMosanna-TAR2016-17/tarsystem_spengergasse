package at.TARsystem.Spengergasse.domain.Entities;

import at.TARsystem.Spengergasse.domain.NonEntities.BaseDomain;
import at.TARsystem.Spengergasse.validation.NotInPastLocalDate;
import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "absenceRequests")
@RequiredArgsConstructor(staticName = "of")
public class AbsenceRequest extends BaseDomain
{
    @Getter
    @Setter
    @NonNull
    @NotNull
    private String requester;

    @Getter
    @Setter
    @NonNull
    @NotNull
    private LocalDateTime rDate;

    @Getter
    @Setter
    @NonNull
    @NotNull
    @NotInPastLocalDate
    private LocalDate absenceDate;

    @Getter
    @Setter
    private String reason;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "request")
    private List<Status> status_history = new ArrayList<>();

    public AbsenceRequest addStatuses(Status... statuses)
    {
        Arrays.stream(statuses).forEach(this::addStatus);
        return this;
    }

    public AbsenceRequest addStatus(Status status)
    {
        Objects.requireNonNull(status);
        this.status_history.add(status);
        status.setRequest(this);
        return this;
    }

    public List<Status> getStatus_history()
    {
        return Collections.unmodifiableList(status_history);
    }

    public Status getLastStatusOfRequest()
    {
        status_history.sort(Comparator.comparing(Status::getTriggerTS).reversed());
        return status_history.get(0);
    }

    public boolean isRequestWaitFor(String waitFor)
    {
        Stream result = status_history.stream().filter(status -> status.getWaitFor() == waitFor);
        if (result.count() > 0)
            return !status_history.stream().anyMatch(status -> status.getWhoTriggered().equalsIgnoreCase(waitFor));
        else
            return false;
    }
}
