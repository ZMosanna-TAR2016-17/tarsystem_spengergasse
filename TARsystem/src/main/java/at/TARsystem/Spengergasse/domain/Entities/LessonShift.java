package at.TARsystem.Spengergasse.domain.Entities;

import at.TARsystem.Spengergasse.domain.NonEntities.BaseDomain;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(staticName = "of")
@Entity
@Table(name = "lessonShifts")
public class LessonShift extends BaseDomain
{
    @NonNull
    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "rId")
    private AbsenceRequest mainRequest;

    @NonNull
    @NotNull
    private String className;

    @NonNull
    @NotNull
    private int absentFrom;

    @NonNull
    @NotNull
    private int absentTo;

    @NonNull
    @NotNull
    private LocalDate returnDate;

    @NonNull
    @NotNull
    private int returnHourFrom;

    @NonNull
    @NotNull
    private int returnHourTo;

}
