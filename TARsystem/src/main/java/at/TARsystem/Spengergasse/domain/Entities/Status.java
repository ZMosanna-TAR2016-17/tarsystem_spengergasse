package at.TARsystem.Spengergasse.domain.Entities;

import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.domain.NonEntities.BaseDomain;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(staticName = "of")
@Entity
@Table(name = "status_histories")
public class Status extends BaseDomain
{

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "rId")
    private AbsenceRequest request;

   /* @NonNull
    @NotNull
    @Enumerated(EnumType.STRING)
    private RequestType requestType;*/

    @NonNull
    @NotNull
    private LocalDateTime triggerTS;

    @Setter
    @NonNull
    @NotNull
    private String whoTriggered;

    @NonNull
    @NotNull
    @Enumerated(EnumType.STRING)
    private UserRole tRole;

    @NonNull
    @NotNull
    @Enumerated(EnumType.STRING)
    private Decision decision;

    private String waitFor;

}
