package at.TARsystem.Spengergasse.domain.NonEntities;

import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import lombok.*;
import org.hibernate.validator.constraints.Email;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(staticName = "of")
public class User
{
   // @NotNull
   // @NonNull
    @Size(min=3)
    private String firstName;

 //   @NotNull
//    @NonNull
    @Size(min=3)
    private String lastName;

    @NotNull
    @NonNull
    @Size(min=3)
    private String userName;

    @Email
    private String email;

    @Size(min=3,max = 4)
    private String ABRV;

    @NotNull
    @NonNull
    @Enumerated(EnumType.STRING)
    private UserRole userRole;
}
