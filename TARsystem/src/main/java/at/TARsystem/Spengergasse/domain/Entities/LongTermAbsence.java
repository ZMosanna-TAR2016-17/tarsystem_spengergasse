package at.TARsystem.Spengergasse.domain.Entities;

import at.TARsystem.Spengergasse.domain.NonEntities.BaseDomain;
import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(staticName = "of")
@Entity
@Table(name = "longTermAbsenceRequests")
public class LongTermAbsence extends BaseDomain
{
    @NonNull
    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    private AbsenceRequest mainRequest;

    @NonNull
    @NotNull
    @Min(1)
    //shold be filled as number of days
    private int estimDuration;

    private int realDuration;
}
