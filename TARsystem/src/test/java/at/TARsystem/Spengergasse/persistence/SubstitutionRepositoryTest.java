package at.TARsystem.Spengergasse.persistence;

import at.TARsystem.Spengergasse.Application;
import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.Entities.Substitution;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.domain.NonEntities.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@SpringApplicationConfiguration(classes = Application.class)
public class SubstitutionRepositoryTest
{
    private static final User testRequester = User.of("mos15809", UserRole.Lehrer);
    private static final User testTrigger1 = User.of("Unger", UserRole.Vertreter);
    private static final User testTrigger2 = User.of("Berger", UserRole.AV);
    private static final LocalDateTime requestingDate = LocalDateTime.now();
    @Autowired
    public SubstitutionRepository subRepo;
    private AbsenceRequest request = AbsenceRequest
            .of(testRequester.getUserName(), requestingDate, requestingDate.toLocalDate().plusDays(7));

    @Before
    public void setup()
    {
        request.setReason("Verlegung");
        Substitution substitution1 = Substitution.of(request, "5ABIF", 5, 6, testTrigger1.getUserName());
        Status st1 = Status.of(request.getRDate(), testRequester.getUserName(), UserRole.Lehrer, Decision.gestartet);
        st1.setWaitFor(testTrigger1.getUserName());
        Status st2 = Status.of(request.getRDate().plusHours(1), testTrigger1
                .getUserName(), UserRole.Vertreter, Decision.akzeptiert);
        st2.setWaitFor(testTrigger2.getUserName());
        request.addStatuses(st1, st2);

        Substitution substitution2 = Substitution.of(request, "5ABIF", 3, 4, testTrigger1.getUserName());

        Substitution substitution3 = Substitution.of(AbsenceRequest.of("Unger", LocalDateTime.now(), LocalDate.now()
                                                                                                              .plusDays(2)), "5ABIF", 1, 2, testRequester
                                                             .getUserName());
        subRepo.save(Arrays.asList(substitution1, substitution2, substitution3));

        assertThat(substitution1.getId(), is(not(nullValue())));
        assertThat(substitution2.getId(), is(not(nullValue())));
        assertThat(substitution3.getId(), is(not(nullValue())));

        assertThat(request.getId(), is(not(nullValue())));
        assertThat(substitution3.getMainRequest().getId(), is(not(nullValue())));
        assertThat(st1.getId(), is(not(nullValue())));
        assertThat(st2.getId(), is(not(nullValue())));
    }

    @Test
    public void testFingById() throws Exception
    {
        Long id = subRepo.findAll().get(0).getId();
        Optional<Substitution> subResult = subRepo.findById(id);

        assertThat(subResult, is(not(nullValue())));
        assertThat(subResult.isPresent(), is(not(nullValue())));

        Substitution substitution = subResult.get();

        assertThat(substitution.getId(), is(not(nullValue())));
        assertThat(substitution.getId(), is(equalTo(id)));
        assertThat(substitution.getMainRequest(), is(sameInstance(request)));
        assertThat(substitution.getAbsentFrom(), is(notNullValue()));
        assertThat(substitution.getAbsentTo(), is(notNullValue()));
        assertThat(substitution.getClassName(), is(notNullValue()));
        assertThat(substitution.getSubstitutedTeacher(), is(notNullValue()));
    }

    @Test
    public void testFindByMainRequest() throws Exception
    {
        List<Substitution> subResult = subRepo.findByMainRequest(request);

        assertThat(subResult, is(not(nullValue())));
        assertThat(subResult.isEmpty(), is(equalTo(false)));

        subResult.forEach(substitution -> {
            assertThat(substitution.getId(), is(not(nullValue())));
            assertThat(substitution.getMainRequest(), is(sameInstance(request)));
            assertThat(substitution.getAbsentFrom(), is(notNullValue()));
            assertThat(substitution.getAbsentTo(), is(notNullValue()));
            assertThat(substitution.getClassName(), is(notNullValue()));
            assertThat(substitution.getSubstitutedTeacher(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByMainRequestAbsenceDate() throws Exception
    {
        List<Substitution> subResult = subRepo.findByMainRequestAbsenceDate(request.getAbsenceDate());

        assertThat(subResult, is(not(nullValue())));
        assertThat(subResult.isEmpty(), is(equalTo(false)));
        assertThat(subResult.size(), is(equalTo(2)));

        subResult.forEach(substitution -> {
            assertThat(substitution.getId(), is(not(nullValue())));
            assertThat(substitution.getMainRequest(), is(sameInstance(request)));
            assertThat(substitution.getMainRequest().getAbsenceDate(), is(equalTo(request.getAbsenceDate())));
            assertThat(substitution.getAbsentFrom(), is(notNullValue()));
            assertThat(substitution.getAbsentTo(), is(notNullValue()));
            assertThat(substitution.getClassName(), is(notNullValue()));
            assertThat(substitution.getSubstitutedTeacher(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByMainRequestId() throws Exception
    {
        List<Substitution> subResult = subRepo.findByMainRequestId(request.getId());

        assertThat(subResult, is(not(nullValue())));
        assertThat(subResult.isEmpty(), is(equalTo(false)));
        assertThat(subResult.size(), is(equalTo(2)));

        subResult.forEach(substitution -> {
            assertThat(substitution.getId(), is(not(nullValue())));
            assertThat(substitution.getMainRequest(), is(sameInstance(request)));
            assertThat(substitution.getMainRequest().getId(), is(equalTo(request.getId())));
            assertThat(substitution.getAbsentFrom(), is(notNullValue()));
            assertThat(substitution.getAbsentTo(), is(notNullValue()));
            assertThat(substitution.getClassName(), is(notNullValue()));
            assertThat(substitution.getSubstitutedTeacher(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByMainRequestRequester() throws Exception
    {
        List<Substitution> subResult = subRepo.findByMainRequestRequester(request.getRequester());

        assertThat(subResult, is(not(nullValue())));
        assertThat(subResult.isEmpty(), is(equalTo(false)));
        assertThat(subResult.size(), is(equalTo(2)));

        subResult.forEach(substitution -> {
            assertThat(substitution.getId(), is(not(nullValue())));
            assertThat(substitution.getMainRequest().getRequester(), is(equalTo(request.getRequester())));
            assertThat(substitution.getAbsentFrom(), is(notNullValue()));
            assertThat(substitution.getAbsentTo(), is(notNullValue()));
            assertThat(substitution.getClassName(), is(notNullValue()));
            assertThat(substitution.getSubstitutedTeacher(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByClassName() throws Exception
    {
        List<Substitution> subResult = subRepo.findByClassName("5ABIF");

        assertThat(subResult, is(not(nullValue())));
        assertThat(subResult.isEmpty(), is(equalTo(false)));
        assertThat(subResult.size(), is(equalTo(3)));

        subResult.forEach(substitution -> {
            assertThat(substitution.getId(), is(not(nullValue())));
            assertThat(substitution.getClassName(), is(notNullValue()));
            assertThat(substitution.getClassName(), is(equalTo("5ABIF")));
            assertThat(substitution.getAbsentFrom(), is(notNullValue()));
            assertThat(substitution.getAbsentTo(), is(notNullValue()));
            assertThat(substitution.getSubstitutedTeacher(), is(notNullValue()));
        });
    }

    @Test
    public void testFindBySubstitutedTeacher() throws Exception
    {
        List<Substitution> subResult = subRepo.findBySubstitutedTeacher("Unger");

        assertThat(subResult, is(not(nullValue())));
        assertThat(subResult.isEmpty(), is(equalTo(false)));
        assertThat(subResult.size(), is(equalTo(2)));

        subResult.forEach(substitution -> {
            assertThat(substitution.getId(), is(not(nullValue())));
            assertThat(substitution.getClassName(), is(notNullValue()));
            assertThat(substitution.getClassName(), is(notNullValue()));
            assertThat(substitution.getAbsentFrom(), is(notNullValue()));
            assertThat(substitution.getAbsentTo(), is(notNullValue()));
            assertThat(substitution.getSubstitutedTeacher(), is(equalTo("Unger")));
        });
    }

    @Test
    public void testFindByMainRequestRDateAfterAndSubstitutedTeacher() throws Exception
    {
        List<Substitution> subResult = subRepo
                .findByMainRequestRDateAfterAndSubstitutedTeacher(requestingDate.minusDays(2), "Unger");

        assertThat(subResult, is(not(nullValue())));
        assertThat(subResult.isEmpty(), is(equalTo(false)));
        assertThat(subResult.size(), is(equalTo(2)));

        subResult.forEach(substitution -> {
            assertThat(substitution.getId(), is(not(nullValue())));
            assertThat(substitution.getClassName(), is(notNullValue()));
            assertThat(substitution.getClassName(), is(notNullValue()));
            assertThat(substitution.getAbsentFrom(), is(notNullValue()));
            assertThat(substitution.getAbsentTo(), is(notNullValue()));
            assertThat(substitution.getSubstitutedTeacher(), is(equalTo("Unger")));
        });
    }
}
