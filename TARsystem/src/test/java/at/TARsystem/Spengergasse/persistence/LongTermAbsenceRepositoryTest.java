package at.TARsystem.Spengergasse.persistence;

import at.TARsystem.Spengergasse.Application;
import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.LongTermAbsence;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.domain.NonEntities.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class LongTermAbsenceRepositoryTest
{
    private static final User testRequester = User.of("mos15809", UserRole.Lehrer);
    private static final User testTrigger1 = User.of("Berger", UserRole.AV);
    private static final User testTrigger2 = User.of("Zemanek", UserRole.SL);
    private static final LocalDateTime requestingDate = LocalDateTime.now();
    private AbsenceRequest request1 = AbsenceRequest
            .of(testRequester.getUserName(), requestingDate, requestingDate.toLocalDate().plusDays(1));

    private AbsenceRequest request2 = AbsenceRequest
            .of(testRequester.getUserName(), requestingDate.plusDays(3), requestingDate.toLocalDate().plusDays(7));

    @Autowired
    public LongTermAbsenceRepository ltaRepo;

    @Before
    public void setup()
    {
        request1.setReason("Krankheit");
        Status st1 = Status.of(request1.getRDate(), testRequester.getUserName(), UserRole.Lehrer, Decision.gestartet);
        st1.setWaitFor(testTrigger1.getUserName());
        Status st2 = Status.of(request1.getRDate().plusHours(1), testTrigger1.getUserName(), UserRole.SL, Decision.akzeptiert);
        st2.setWaitFor(testTrigger2.getUserName());
        request1.addStatuses(st1, st2);

        request2.setReason("Krankheit");
        Status st21 = Status.of(request2.getRDate(), testRequester.getUserName(), UserRole.Lehrer, Decision.gestartet);
        st21.setWaitFor(testTrigger1.getUserName());
        Status st22 = Status.of(request2.getRDate().plusHours(1), testTrigger1.getUserName(), UserRole.SL, Decision.abgelehnt);
        st22.setWaitFor(null);
        request2.addStatuses(st21, st22);

        LongTermAbsence lta1 = LongTermAbsence.of(request1,2);
        LongTermAbsence lta2 = LongTermAbsence.of(request2,4);

        ltaRepo.save(Arrays.asList(lta1, lta2));

        assertThat(request1.getId(), is(not(nullValue())));
        assertThat(request2.getId(), is(not(nullValue())));
        assertThat(st1.getId(), is(not(nullValue())));
        assertThat(st2.getId(), is(not(nullValue())));
        assertThat(st21.getId(), is(not(nullValue())));
        assertThat(st22.getId(), is(not(nullValue())));
        assertThat(lta1.getId(), is(not(nullValue())));
        assertThat(lta1.getId(), is(not(nullValue())));

    }

    @Test
    public void testFindById() throws Exception
    {
        Long id = ltaRepo.findAll().get(0).getId();
        Optional<LongTermAbsence> ltaResult = ltaRepo.findById(id);
        assertThat(ltaResult, is(not(nullValue())));
        assertThat(ltaResult.isPresent(), is(not(nullValue())));

        LongTermAbsence longTermAbsence = ltaResult.get();

        assertThat(longTermAbsence.getId(), is(not(nullValue())));
        assertThat(longTermAbsence.getId(), is(equalTo(id)));
        assertThat(longTermAbsence.getMainRequest(), is(not(nullValue())));
        assertThat(longTermAbsence.getEstimDuration(), is(not(nullValue())));
        assertThat(longTermAbsence.getEstimDuration(), is(greaterThanOrEqualTo(1)));
    }

    @Test
    public void testFindByMainRequest() throws Exception
    {
        List<LongTermAbsence> ltaResult = ltaRepo.findByMainRequest(request1);

        assertThat(ltaResult, is(not(nullValue())));
        assertThat(ltaResult.isEmpty(), is(not(true)));
        assertThat(ltaResult.size(),is(equalTo(1)));

        ltaResult.forEach(longTermAbsence -> {
            assertThat(longTermAbsence.getId(), is(not(nullValue())));
            assertThat(longTermAbsence.getMainRequest(), is(not(nullValue())));
            assertThat(longTermAbsence.getMainRequest(), is(sameInstance(request1)));
            assertThat(longTermAbsence.getEstimDuration(), is(not(nullValue())));
            assertThat(longTermAbsence.getEstimDuration(), is(greaterThanOrEqualTo(1)));
        });
    }

    @Test
    public void testFindByMainRequestAbsenceDate() throws Exception
    {
        List<LongTermAbsence> ltaResult = ltaRepo.findByMainRequestAbsenceDate(request1.getAbsenceDate());

        assertThat(ltaResult, is(not(nullValue())));
        assertThat(ltaResult.isEmpty(), is(not(true)));
        assertThat(ltaResult.size(),is(equalTo(1)));

        ltaResult.forEach(longTermAbsence -> {
            assertThat(longTermAbsence.getId(), is(not(nullValue())));
            assertThat(longTermAbsence.getMainRequest(), is(not(nullValue())));
            assertThat(longTermAbsence.getMainRequest().getAbsenceDate(), is(equalTo(request1.getAbsenceDate())));
            assertThat(longTermAbsence.getEstimDuration(), is(not(nullValue())));
            assertThat(longTermAbsence.getEstimDuration(), is(greaterThanOrEqualTo(1)));
        });
    }

    @Test
    public void testFindByMainRequestId() throws Exception
    {
        List<LongTermAbsence> ltaResult = ltaRepo.findByMainRequestId(request1.getId());

        assertThat(ltaResult, is(not(nullValue())));
        assertThat(ltaResult.isEmpty(), is(not(true)));
        assertThat(ltaResult.size(),is(equalTo(1)));

        ltaResult.forEach(longTermAbsence -> {
            assertThat(longTermAbsence.getId(), is(not(nullValue())));
            assertThat(longTermAbsence.getMainRequest(), is(not(nullValue())));
            assertThat(longTermAbsence.getMainRequest().getId(), is(equalTo(request1.getId())));
            assertThat(longTermAbsence.getEstimDuration(), is(not(nullValue())));
            assertThat(longTermAbsence.getEstimDuration(), is(greaterThanOrEqualTo(1)));
        });
    }

    @Test
    public void testFindByMainRequestRequester() throws Exception
    {
        List<LongTermAbsence> ltaResult = ltaRepo.findByMainRequestRequester(testRequester.getUserName());

        assertThat(ltaResult, is(not(nullValue())));
        assertThat(ltaResult.isEmpty(), is(not(true)));
        assertThat(ltaResult.size(),is(equalTo(2)));

        ltaResult.forEach(longTermAbsence -> {
            assertThat(longTermAbsence.getId(), is(not(nullValue())));
            assertThat(longTermAbsence.getMainRequest(), is(not(nullValue())));
            assertThat(longTermAbsence.getMainRequest().getRequester(), is(equalTo(testRequester.getUserName())));
            assertThat(longTermAbsence.getEstimDuration(), is(not(nullValue())));
            assertThat(longTermAbsence.getEstimDuration(), is(greaterThanOrEqualTo(1)));
        });
    }

}
