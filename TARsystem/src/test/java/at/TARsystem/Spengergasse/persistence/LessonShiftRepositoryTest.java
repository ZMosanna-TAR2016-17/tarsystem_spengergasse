package at.TARsystem.Spengergasse.persistence;

import at.TARsystem.Spengergasse.Application;
import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.LessonShift;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.domain.NonEntities.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class LessonShiftRepositoryTest
{
    private static final User testRequester = User.of("mos15809", UserRole.Lehrer);
    private static final User testTrigger1 = User.of("Unger", UserRole.Vertreter);
    private static final User testTrigger2 = User.of("Berger", UserRole.AV);
    private static final LocalDateTime requestingDate = LocalDateTime.now();
    @Autowired
    public LessonShiftRepository lsRepo;
    private AbsenceRequest request = AbsenceRequest
            .of(testRequester.getUserName(), requestingDate, requestingDate.toLocalDate().plusDays(7));

    @Before
    public void setup()
    {
        request.setReason("Verlegung");
        Status st1 = Status.of(request.getRDate(), testRequester.getUserName(), UserRole.Lehrer, Decision.gestartet);
        st1.setWaitFor(testTrigger1.getUserName());
        Status st2 = Status.of(request.getRDate().plusHours(1), testTrigger1
                .getUserName(), UserRole.Vertreter, Decision.akzeptiert);
        st2.setWaitFor(testTrigger2.getUserName());
        request.addStatuses(st1, st2);

        LessonShift lessonShift1 = LessonShift
                .of(request, "5ABIF", 5, 6, requestingDate.plusDays(12).toLocalDate(), 5, 6);
        LessonShift lessonShift2 = LessonShift
                .of(request, "5ABIF", 3, 4, requestingDate.plusDays(10).toLocalDate(), 3, 4);
        LessonShift lessonShift3 = LessonShift.of(AbsenceRequest.of("Unger", LocalDateTime.now(), LocalDate.now().plusDays(2)),"5ABIF",1, 2, requestingDate.plusDays(8).toLocalDate(), 3, 4);
        lsRepo.save(Arrays.asList(lessonShift1, lessonShift2,lessonShift3));

        assertThat(lessonShift1.getId(), is(not(nullValue())));
        assertThat(lessonShift2.getId(), is(not(nullValue())));
        assertThat(lessonShift3.getId(), is(not(nullValue())));

        assertThat(request.getId(), is(not(nullValue())));
        assertThat(st1.getId(), is(not(nullValue())));
        assertThat(st2.getId(), is(not(nullValue())));
    }

    @Test
    public void testFingById() throws Exception
    {
        Long id = lsRepo.findAll().get(0).getId();
        Optional<LessonShift> lsResult = lsRepo.findById(id);

        assertThat(lsResult, is(not(nullValue())));
        assertThat(lsResult.isPresent(), is(not(nullValue())));

        LessonShift lessonShift = lsResult.get();

        assertThat(lessonShift.getId(), is(not(nullValue())));
        assertThat(lessonShift.getId(), is(equalTo(id)));
        assertThat(lessonShift.getMainRequest(), is(sameInstance(request)));
        assertThat(lessonShift.getReturnDate(), is(notNullValue()));
        assertThat(lessonShift.getAbsentFrom(), is(notNullValue()));
        assertThat(lessonShift.getAbsentTo(), is(notNullValue()));
        assertThat(lessonShift.getClassName(), is(notNullValue()));
        assertThat(lessonShift.getReturnHourFrom(), is(notNullValue()));
        assertThat(lessonShift.getReturnHourTo(), is(notNullValue()));
    }

    @Test
    public void testFindByMainRequest() throws Exception
    {
        List<LessonShift> lsResult = lsRepo.findByMainRequest(request);

        assertThat(lsResult, is(not(nullValue())));
        assertThat(lsResult.isEmpty(), is(equalTo(false)));

        lsResult.forEach(lessonShift -> {
            assertThat(lessonShift.getId(), is(not(nullValue())));
            assertThat(lessonShift.getMainRequest(), is(sameInstance(request)));
            assertThat(lessonShift.getReturnDate(), is(notNullValue()));
            assertThat(lessonShift.getAbsentFrom(), is(notNullValue()));
            assertThat(lessonShift.getAbsentTo(), is(notNullValue()));
            assertThat(lessonShift.getClassName(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourFrom(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourTo(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByMainRequestAbsenceDate() throws Exception
    {
        List<LessonShift> lsResult = lsRepo.findByMainRequestAbsenceDate(request.getAbsenceDate());

        assertThat(lsResult, is(not(nullValue())));
        assertThat(lsResult.isEmpty(), is(equalTo(false)));
        assertThat(lsResult.size(), is(equalTo(2)));

        lsResult.forEach(lessonShift -> {
            assertThat(lessonShift.getId(), is(not(nullValue())));
            assertThat(lessonShift.getMainRequest(), is(sameInstance(request)));
            assertThat(lessonShift.getMainRequest().getAbsenceDate(), is(equalTo(request.getAbsenceDate())));
            assertThat(lessonShift.getReturnDate(), is(notNullValue()));
            assertThat(lessonShift.getAbsentFrom(), is(notNullValue()));
            assertThat(lessonShift.getAbsentTo(), is(notNullValue()));
            assertThat(lessonShift.getClassName(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourFrom(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourTo(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByMainRequestId() throws Exception
    {
        List<LessonShift> lsResult = lsRepo.findByMainRequestId(request.getId());

        assertThat(lsResult, is(not(nullValue())));
        assertThat(lsResult.isEmpty(), is(equalTo(false)));
        assertThat(lsResult.size(), is(equalTo(2)));

        lsResult.forEach(lessonShift -> {
            assertThat(lessonShift.getId(), is(not(nullValue())));
            assertThat(lessonShift.getMainRequest(), is(sameInstance(request)));
            assertThat(lessonShift.getMainRequest().getId(), is(equalTo(request.getId())));
            assertThat(lessonShift.getReturnDate(), is(notNullValue()));
            assertThat(lessonShift.getAbsentFrom(), is(notNullValue()));
            assertThat(lessonShift.getAbsentTo(), is(notNullValue()));
            assertThat(lessonShift.getClassName(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourFrom(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourTo(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByMainRequestRequester() throws Exception
    {
        List<LessonShift> lsResult = lsRepo.findByMainRequestRequester(request.getRequester());

        assertThat(lsResult, is(not(nullValue())));
        assertThat(lsResult.isEmpty(), is(equalTo(false)));
        assertThat(lsResult.size(), is(equalTo(2)));

        lsResult.forEach(lessonShift -> {
            assertThat(lessonShift.getId(), is(not(nullValue())));
            assertThat(lessonShift.getMainRequest().getRequester(), is(equalTo(request.getRequester())));
            assertThat(lessonShift.getReturnDate(), is(notNullValue()));
            assertThat(lessonShift.getAbsentFrom(), is(notNullValue()));
            assertThat(lessonShift.getAbsentTo(), is(notNullValue()));
            assertThat(lessonShift.getClassName(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourFrom(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourTo(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByMainRequestRequesterAndClassName() throws Exception
    {
        List<LessonShift> lsResult = lsRepo.findByMainRequestRequesterAndClassName(request.getRequester(), "5ABIF");

        assertThat(lsResult, is(not(nullValue())));
        assertThat(lsResult.isEmpty(), is(equalTo(false)));
        assertThat(lsResult.size(), is(equalTo(2)));

        lsResult.forEach(lessonShift -> {
            assertThat(lessonShift.getId(), is(not(nullValue())));
            assertThat(lessonShift.getClassName(), is(equalTo("5ABIF")));
            assertThat(lessonShift.getMainRequest().getRequester(), is(equalTo(request.getRequester())));
            assertThat(lessonShift.getReturnDate(), is(notNullValue()));
            assertThat(lessonShift.getAbsentFrom(), is(notNullValue()));
            assertThat(lessonShift.getAbsentTo(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourFrom(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourTo(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByClassName() throws Exception
    {
        List<LessonShift> lsResult = lsRepo.findByClassName("5ABIF");

        assertThat(lsResult, is(not(nullValue())));
        assertThat(lsResult.isEmpty(), is(equalTo(false)));
        assertThat(lsResult.size(), is(equalTo(3)));

        lsResult.forEach(lessonShift -> {
            assertThat(lessonShift.getId(), is(not(nullValue())));
            assertThat(lessonShift.getClassName(), is(notNullValue()));
            assertThat(lessonShift.getClassName(), is(equalTo("5ABIF")));
            assertThat(lessonShift.getReturnDate(), is(notNullValue()));
            assertThat(lessonShift.getAbsentFrom(), is(notNullValue()));
            assertThat(lessonShift.getAbsentTo(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourFrom(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourTo(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByReturnDate() throws Exception
    {
        List<LessonShift> lsResult = lsRepo.findByReturnDate(requestingDate.plusDays(12).toLocalDate());

        assertThat(lsResult, is(not(nullValue())));
        assertThat(lsResult.isEmpty(), is(equalTo(false)));
        assertThat(lsResult.size(), is(equalTo(1)));

        lsResult.forEach(lessonShift -> {
            assertThat(lessonShift.getId(), is(not(nullValue())));
            assertThat(lessonShift.getReturnDate(), is(notNullValue()));
            assertThat(lessonShift.getReturnDate(), is(equalTo(requestingDate.plusDays(12).toLocalDate())));
            assertThat(lessonShift.getAbsentFrom(), is(notNullValue()));
            assertThat(lessonShift.getAbsentTo(), is(notNullValue()));
            assertThat(lessonShift.getClassName(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourFrom(), is(notNullValue()));
            assertThat(lessonShift.getReturnHourTo(), is(notNullValue()));
        });
    }

}
