package at.TARsystem.Spengergasse.persistence;

import at.TARsystem.Spengergasse.Application;
import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.domain.NonEntities.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class AbsenceRequestRepositoryTest
{
    private static final User testRequester = User.of("mos15809", UserRole.Lehrer);
    private static final User testTrigger1 = User.of("Unger", UserRole.Vertreter);
    private static final User testTrigger2 = User.of("Berger", UserRole.AV);
    private static final LocalDateTime requestingDate = LocalDateTime.now();
    @Autowired
    public AbsenceRequestRepository arRepo;

    @Before
    public void setup()
    {

        AbsenceRequest request = AbsenceRequest
                .of(testRequester.getUserName(), requestingDate, requestingDate.toLocalDate().plusDays(7));
        request.setReason("Verlegung");
        Status st1 = Status.of(request.getRDate(), testRequester.getUserName(), UserRole.Lehrer, Decision.gestartet);
        st1.setWaitFor(testTrigger1.getUserName());
        Status st2 = Status.of(request.getRDate().plusHours(1), testTrigger1
                .getUserName(), UserRole.Vertreter, Decision.akzeptiert);
        st2.setWaitFor(testTrigger2.getUserName());

        request.addStatuses(st1, st2);

        AbsenceRequest request2 = AbsenceRequest
                .of(testRequester.getUserName(), requestingDate.minusDays(1), requestingDate.toLocalDate().plusDays(7));
        request2.setReason("Verlegung");
        Status st21 = Status.of(request2.getRDate(), testRequester.getUserName(), UserRole.Lehrer, Decision.gestartet);
        st21.setWaitFor(testTrigger1.getUserName());
        Status st22 = Status.of(request2.getRDate().plusHours(1), testTrigger1
                .getUserName(), UserRole.Vertreter, Decision.abgelehnt);
        st22.setWaitFor(null);
        request2.addStatuses(st21, st22);
        arRepo.save(Arrays.asList(request,request2));

        assertThat(request.getId(), is(not(nullValue())));
        assertThat(request2.getId(), is(not(nullValue())));
        assertThat(st1.getId(), is(not(nullValue())));
        assertThat(st2.getId(), is(not(nullValue())));
        assertThat(st21.getId(), is(not(nullValue())));
        assertThat(st22.getId(), is(not(nullValue())));
    }

    @Test
    public void testFindById() throws Exception
    {
        Long id = arRepo.findAll().get(0).getId();
        Optional<AbsenceRequest> arResult = arRepo.findById(id);
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isPresent(), is(not(nullValue())));

        AbsenceRequest request = arResult.get();

        assertThat(request.getId(), is(not(nullValue())));
        assertThat(request.getId(), is(equalTo(id)));
        assertThat(request.getRequester(), is(equalTo(testRequester.getUserName())));
        request.getStatus_history().forEach(status -> {
            assertThat(status, is(not(nullValue())));
            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(sameInstance(request)));
            assertThat(status.getTriggerTS(), is(not(nullValue())));
            assertThat(status.getWhoTriggered(), is(not(nullValue())));
            assertThat(status.getTRole(), is(not(nullValue())));
            assertThat(status.getDecision(), is(not(nullValue())));
        });
    }

    @Test
    public void testFindByRDate() throws Exception
    {
        List<AbsenceRequest> arResult = arRepo.findByRDate(requestingDate);
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isEmpty(), is(equalTo(false)));

        arResult.forEach(request -> {
            assertThat(request.getId(), is(not(nullValue())));
            assertThat(request.getRDate(), is(equalTo(requestingDate)));
            request.getStatus_history().forEach(status -> {
                assertThat(status, is(not(nullValue())));
                assertThat(status.getId(), is(not(nullValue())));
                assertThat(status.getRequest(), is(sameInstance(request)));
                assertThat(status.getTriggerTS(), is(not(nullValue())));
                assertThat(status.getWhoTriggered(), is(not(nullValue())));
                assertThat(status.getTRole(), is(not(nullValue())));
                assertThat(status.getDecision(), is(not(nullValue())));
            });
        });
    }

    @Test
    public void testFindByRequester() throws Exception
    {
        List<AbsenceRequest> arResult = arRepo.findByRequester(testRequester.getUserName());
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isEmpty(), is(equalTo(false)));
        assertThat(arResult.size(), is(equalTo(2)));

        arResult.forEach(request -> {
            assertThat(request.getId(), is(not(nullValue())));
            assertThat(request.getRequester(), is(equalTo(testRequester.getUserName())));
            request.getStatus_history().forEach(status -> {
                assertThat(status, is(not(nullValue())));
                assertThat(status.getId(), is(not(nullValue())));
                assertThat(status.getRequest(), is(sameInstance(request)));
                assertThat(status.getTriggerTS(), is(not(nullValue())));
                assertThat(status.getWhoTriggered(), is(not(nullValue())));
                assertThat(status.getTRole(), is(not(nullValue())));
                assertThat(status.getDecision(), is(not(nullValue())));
            });
        });
    }

    @Test
    public void testFindByAbsenceDate() throws Exception
    {
        List<AbsenceRequest> arResult = arRepo
                .findByRequesterAndAbsenceDate(testRequester.getUserName(), requestingDate.toLocalDate().plusDays(7));
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isEmpty(), is(equalTo(false)));
        assertThat(arResult.size(), is(equalTo(2)));

        arResult.forEach(request -> {
            assertThat(request.getId(), is(not(nullValue())));
            assertThat(request.getRequester(), is(equalTo(testRequester.getUserName())));
            assertThat(request.getAbsenceDate(), is(equalTo(requestingDate.toLocalDate().plusDays(7))));
            request.getStatus_history().forEach(status -> {
                assertThat(status, is(not(nullValue())));
                assertThat(status.getId(), is(not(nullValue())));
                assertThat(status.getRequest(), is(sameInstance(request)));
                assertThat(status.getTriggerTS(), is(not(nullValue())));
                assertThat(status.getWhoTriggered(), is(not(nullValue())));
                assertThat(status.getTRole(), is(not(nullValue())));
                assertThat(status.getDecision(), is(not(nullValue())));
            });
        });
    }

    @Test
    public void testFindByRequesterAndRDateAfter() throws Exception
    {
        List<AbsenceRequest> arResult = arRepo
                .findByRequesterAndRDateAfter(testRequester.getUserName(), requestingDate.minusDays(5));
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isEmpty(), is(equalTo(false)));
        assertThat(arResult.size(), is(equalTo(2)));

        arResult.forEach(request -> {
            assertThat(request.getId(), is(not(nullValue())));
            assertThat(request.getRequester(), is(equalTo(testRequester.getUserName())));
            assertThat(request.getRDate(), is(greaterThan(requestingDate.minusDays(5))));
            request.getStatus_history().forEach(status -> {
                assertThat(status, is(not(nullValue())));
                assertThat(status.getId(), is(not(nullValue())));
                assertThat(status.getRequest(), is(sameInstance(request)));
                assertThat(status.getTriggerTS(), is(not(nullValue())));
                assertThat(status.getWhoTriggered(), is(not(nullValue())));
                assertThat(status.getTRole(), is(not(nullValue())));
                assertThat(status.getDecision(), is(not(nullValue())));
            });
        });
    }

    @Test
    public void testFindByRequesterAndRDateBefore() throws Exception
    {
        List<AbsenceRequest> arResult = arRepo
                .findByRequesterAndRDateBefore(testRequester.getUserName(), requestingDate.plusDays(5));
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isEmpty(), is(equalTo(false)));
        assertThat(arResult.size(), is(equalTo(2)));

        arResult.forEach(request -> {
            assertThat(request.getId(), is(not(nullValue())));
            assertThat(request.getRequester(), is(equalTo(testRequester.getUserName())));
            assertThat(request.getRDate(), is(lessThan(requestingDate.plusDays(5))));
            request.getStatus_history().forEach(status -> {
                assertThat(status, is(not(nullValue())));
                assertThat(status.getId(), is(not(nullValue())));
                assertThat(status.getRequest(), is(sameInstance(request)));
                assertThat(status.getTriggerTS(), is(not(nullValue())));
                assertThat(status.getWhoTriggered(), is(not(nullValue())));
                assertThat(status.getTRole(), is(not(nullValue())));
                assertThat(status.getDecision(), is(not(nullValue())));
            });
        });
    }

    @Test
    public void testFindByRequesterAndRDateBetween() throws Exception
    {
        List<AbsenceRequest> arResult = arRepo
                .findByRequesterAndRDateBetween(testRequester.getUserName(),
                                                requestingDate.minusDays(5),
                                                requestingDate.plusDays(5));
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isEmpty(), is(equalTo(false)));
        assertThat(arResult.size(), is(equalTo(2)));

        arResult.forEach(request -> {
            assertThat(request.getId(), is(not(nullValue())));
            assertThat(request.getRequester(), is(equalTo(testRequester.getUserName())));
            assertThat(request.getRDate(), is(greaterThan(requestingDate.minusDays(5))));
            assertThat(request.getRDate(), is(lessThan(requestingDate.plusDays(5))));
            request.getStatus_history().forEach(status -> {
                assertThat(status, is(not(nullValue())));
                assertThat(status.getId(), is(not(nullValue())));
                assertThat(status.getRequest(), is(sameInstance(request)));
                assertThat(status.getTriggerTS(), is(not(nullValue())));
                assertThat(status.getWhoTriggered(), is(not(nullValue())));
                assertThat(status.getTRole(), is(not(nullValue())));
                assertThat(status.getDecision(), is(not(nullValue())));
            });
        });
    }

    @Test
    public void testFindByRequesterAndStatusTRoleAndStatusDesition() throws Exception
    {
        List<AbsenceRequest> arResult = arRepo.findByRequesterAndStatusTRoleAndStatusDesition(testRequester
                                                                                                      .getUserName(),
                                                                                              UserRole.Vertreter,
                                                                                              Decision.abgelehnt);
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isEmpty(), is(equalTo(false)));
        assertThat(arResult.size(), is(equalTo(1)));

        arResult.forEach(request -> {
            assertThat(request.getId(), is(not(nullValue())));
            assertThat(request.getRequester(), is(equalTo(testRequester.getUserName())));
            int caunt = 0;
            request.getStatus_history().stream()
                   .filter(s -> s.getTRole() == UserRole.Vertreter && s.getDecision() == Decision.abgelehnt)
                   .forEach(status -> {
                       assertThat(status, is(not(nullValue())));
                       assertThat(status.getId(), is(not(nullValue())));
                       assertThat(status.getRequest(), is(sameInstance(request)));
                       assertThat(status.getTriggerTS(), is(not(nullValue())));
                       assertThat(status.getWhoTriggered(), is(not(nullValue())));
                       assertThat(status.getTRole(), is(equalTo(UserRole.Vertreter)));
                       assertThat(status.getDecision(), is(equalTo(Decision.abgelehnt)));
                   });
        });
    }

    @Test
    public void testFindByLastStatusWaitFor() throws Exception
    {
        List<AbsenceRequest> arResult = arRepo.findByLastStatusWaitFor(testTrigger2.getUserName());
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isEmpty(), is(equalTo(false)));
        assertThat(arResult.size(), is(equalTo(1)));

        arResult.forEach(request -> {
            assertThat(request.getId(), is(not(nullValue())));
            assertThat(request.getRequester(), is(equalTo(testRequester.getUserName())));
            request.getStatus_history().stream()
                   .filter(s -> s.getWaitFor() == testTrigger2.getUserName())
                   .forEach(status -> {
                       assertThat(status, is(not(nullValue())));
                       assertThat(status.getId(), is(not(nullValue())));
                       assertThat(status.getRequest(), is(sameInstance(request)));
                       assertThat(status.getTriggerTS(), is(not(nullValue())));
                       assertThat(status.getWhoTriggered(), is(not(nullValue())));
                       assertThat(status.getWaitFor(), is(equalTo(testTrigger2.getUserName())));
                   });
        });
    }

    @Test
    public void testFindByStatusTrigger() throws Exception
    {
        List<AbsenceRequest> arResult = arRepo.findByStatusWhoTriggered(testTrigger1.getUserName());
        assertThat(arResult, is(not(nullValue())));
        assertThat(arResult.isEmpty(), is(equalTo(false)));
        assertThat(arResult.size(), is(equalTo(2)));

        arResult.forEach(request -> {
            assertThat(request.getId(), is(not(nullValue())));
            assertThat(request.getRequester(), is(equalTo(testRequester.getUserName())));
            request.getStatus_history().stream()
                   .filter(s -> s.getWhoTriggered() == testTrigger1.getUserName())
                   .forEach(status -> {
                       assertThat(status, is(not(nullValue())));
                       assertThat(status.getId(), is(not(nullValue())));
                       assertThat(status.getRequest(), is(sameInstance(request)));
                       assertThat(status.getTriggerTS(), is(not(nullValue())));
                       assertThat(status.getWhoTriggered(), is(not(nullValue())));
                       assertThat(status.getWhoTriggered(), is(equalTo(testTrigger1.getUserName())));
                   });
        });
    }
}

