package at.TARsystem.Spengergasse.persistence;

import at.TARsystem.Spengergasse.Application;
import at.TARsystem.Spengergasse.domain.Entities.AbsenceRequest;
import at.TARsystem.Spengergasse.domain.Entities.Status;
import at.TARsystem.Spengergasse.domain.EnumTypes.Decision;
import at.TARsystem.Spengergasse.domain.EnumTypes.UserRole;
import at.TARsystem.Spengergasse.domain.NonEntities.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@SpringApplicationConfiguration(classes = Application.class)
public class StatusRepositoryTest
{
    private static final User testRequester = User.of("mos15809", UserRole.Lehrer);
    private static final User testTrigger1 = User.of("Unger", UserRole.Vertreter);
    private static final User testTrigger2 = User.of("Berger", UserRole.AV);
    private static final LocalDateTime requestingDate = LocalDateTime.now();
    private AbsenceRequest request = AbsenceRequest
            .of(testRequester.getUserName(), requestingDate, requestingDate.toLocalDate().plusDays(7));
    private AbsenceRequest request2 = AbsenceRequest
            .of(testRequester.getUserName(), requestingDate.minusDays(1), requestingDate.toLocalDate().plusDays(10));
    @Autowired
    public StatusRepository stRepo;

    @Before
    public void setup()
    {
        request.setReason("Verlegung");
        Status st1 = Status.of(request.getRDate(), testRequester.getUserName(), UserRole.Lehrer, Decision.gestartet);
        st1.setWaitFor(testTrigger1.getUserName());
        Status st2 = Status.of(request.getRDate().plusHours(1), testTrigger1
                .getUserName(), UserRole.Vertreter, Decision.akzeptiert);
        st2.setWaitFor(testTrigger2.getUserName());
        request.addStatuses(st1, st2);

        request2.setReason("Verlegung");
        Status st21 = Status.of(request2.getRDate(), testRequester.getUserName(), UserRole.Lehrer, Decision.gestartet);
        st21.setWaitFor(testTrigger1.getUserName());
        Status st22 = Status.of(request2.getRDate().plusHours(1), testTrigger1
                .getUserName(), UserRole.Vertreter, Decision.abgelehnt);
        st22.setWaitFor(null);
        request2.addStatuses(st21, st22);
        stRepo.save(Arrays.asList(st1, st2, st21, st22));

        assertThat(request.getId(), is(not(nullValue())));
        assertThat(request2.getId(), is(not(nullValue())));
        assertThat(st1.getId(), is(not(nullValue())));
        assertThat(st2.getId(), is(not(nullValue())));
        assertThat(st21.getId(), is(not(nullValue())));
        assertThat(st22.getId(), is(not(nullValue())));
    }

    @Test
    public void testFindById() throws Exception
    {
        Long id = stRepo.findAll().get(0).getId();
        Optional<Status> stResult = stRepo.findById(id);

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isPresent(), is(not(nullValue())));

        Status status = stResult.get();

        assertThat(status.getId(), is(not(nullValue())));
        assertThat(status.getId(), is(equalTo(id)));
        assertThat(status.getRequest(), is(notNullValue()));
        assertThat(status.getTriggerTS(), is(notNullValue()));
        assertThat(status.getWhoTriggered(), is(notNullValue()));
        assertThat(status.getTRole(), is(notNullValue()));
        assertThat(status.getDecision(), is(notNullValue()));
        assertThat(status.getWaitFor(), is(notNullValue()));
    }

    @Test
    public void testFindByRequest() throws Exception
    {
        List<Status> stResult = stRepo.findByRequest(request);

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(2)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getRequest(), is(sameInstance(request)));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByRequestAbsenceDate() throws Exception
    {
        List<Status> stResult = stRepo.findByRequestAbsenceDate(request.getAbsenceDate());

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(2)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getRequest().getAbsenceDate(), is(equalTo(request.getAbsenceDate())));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByRequestId() throws Exception
    {
        List<Status> stResult = stRepo.findByRequestId(request.getId());

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(2)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getRequest().getId(), is(equalTo(request.getId())));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByRequestRequester() throws Exception
    {
        List<Status> stResult = stRepo.findByRequestRequester(request.getRequester());

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(4)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getRequest().getRequester(), is(equalTo(request.getRequester())));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByTriggerTS() throws Exception
    {
        List<Status> stResult = stRepo.findByTriggerTS(request.getRDate());

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(1)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getTriggerTS(), is(equalTo(request.getRDate())));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByWhoTriggered() throws Exception
    {
        List<Status> stResult = stRepo.findByWhoTriggered(testTrigger1.getUserName());

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(2)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(equalTo(testTrigger1.getUserName())));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByWhoTriggeredAndRequestRDateAfter() throws Exception
    {
        List<Status> stResult = stRepo
                .findByWhoTriggeredAndRequestRDateAfter(testTrigger1.getUserName(), requestingDate.minusDays(2));

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(2)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getRequest().getRDate(), is(greaterThan(requestingDate.minusDays(2))));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(equalTo(testTrigger1.getUserName())));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByfindByWhoTriggeredAndRequestRDateBefore() throws Exception
    {
        List<Status> stResult = stRepo
                .findByWhoTriggeredAndRequestRDateBefore(testTrigger1.getUserName(), requestingDate.plusDays(1));

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(2)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getRequest().getRDate(), is(lessThan(requestingDate.plusDays(1))));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(equalTo(testTrigger1.getUserName())));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByWhoTriggeredAndRequestRDateBetween() throws Exception
    {
        List<Status> stResult = stRepo
                .findByWhoTriggeredAndRequestRDateBetween(testTrigger1.getUserName(), requestingDate
                        .minusDays(2), requestingDate.plusDays(1));

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(2)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getRequest().getRDate(), is(greaterThan(requestingDate.minusDays(2))));
            assertThat(status.getRequest().getRDate(), is(lessThan(requestingDate.plusDays(1))));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(equalTo(testTrigger1.getUserName())));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
        });
    }

    @Test
    public void testFindByLastStatusWaitFor() throws Exception
    {
        List<Status> stResult = stRepo.findByLastStatusWaitFor(testTrigger2.getUserName());

        assertThat(stResult, is(not(nullValue())));
        assertThat(stResult.isEmpty(), is(not(true)));
        assertThat(stResult.size(), is(equalTo(1)));

        stResult.forEach(status -> {

            assertThat(status.getId(), is(not(nullValue())));
            assertThat(status.getRequest(), is(notNullValue()));
            assertThat(status.getTriggerTS(), is(notNullValue()));
            assertThat(status.getWhoTriggered(), is(notNullValue()));
            assertThat(status.getTRole(), is(notNullValue()));
            assertThat(status.getDecision(), is(notNullValue()));
            assertThat(status.getWaitFor(),is(notNullValue()));
            assertThat(status.getWaitFor(),is(equalTo(testTrigger2.getUserName())));
        });
    }
}

